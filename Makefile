#!/usr/bin/make -f

# This file is part of the  X2Go Project - http://www.x2go.org
# Copyright (C) 2012-2015 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# X2Go Session Broker is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# X2Go Session Broker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

SRC_DIR=$(CURDIR)
SHELL=/bin/bash

INSTALL_DIR=install -dm 755
INSTALL_FILE=install -m 644
INSTALL_PROGRAM=install -m 755
INSTALL_SYMLINK=ln -sf

CC      ?= gcc
CFLAGS  += -fPIE
LDFLAGS += -pie

RM_FILE=rm -f
RM_DIR=rmdir -p --ignore-fail-on-non-empty

DESTDIR ?=
PREFIX ?= /usr/local
ETCDIR=/etc/x2go
BINDIR=$(PREFIX)/bin
SBINDIR=$(PREFIX)/sbin
LIBDIR=$(PREFIX)/lib/x2go
MANDIR=$(PREFIX)/share/man
SHAREDIR=$(PREFIX)/share/x2go

BIN_SCRIPTS=$(shell cd bin && echo *)
SBIN_SCRIPTS=$(shell cd sbin && echo *)
LIB_FILES=$(shell cd lib && echo *)

PERL ?= /usr/bin/perl

all: clean build

build: build-arch build-indep

build-arch: build_setuidwrappers

build_setuidwrappers:
	$(CC) $(CFLAGS) $(LDFLAGS) -DTRUSTED_BINARY=\"$(LIBDIR)/x2gobroker-agent.pl\" -o lib/x2gobroker-agent src/x2gobroker-agent.c
	$(CC) $(CFLAGS) $(LDFLAGS) -DTRUSTED_BINARY=\"$(BINDIR)/x2gobroker\" -o bin/x2gobroker-ssh src/x2gobroker-ssh.c

build-indep:

distclean: clean

clean: clean_arch clean_indep

clean_arch:
	$(RM_FILE) lib/x2gobroker-agent

clean_indep:

test:

install:
	mkdir -p "${DESTDIR}/var/lib/x2gobroker" \
	        "${DESTDIR}/var/log/x2gobroker"

	# python-x2gobroker
	python setup.py install --prefix="${PREFIX}" $${DESTDIR+--root="${DESTDIR}"}
	mkdir -p "${DESTDIR}${ETCDIR}/broker" "${DESTDIR}/etc/pam.d" \
	        "${DESTDIR}/etc/default"
	${INSTALL_FILE} defaults/python-x2gobroker.default \
	        "${DESTDIR}/etc/default/python-x2gobroker"
	${INSTALL_FILE} etc/x2gobroker.conf "${DESTDIR}${ETCDIR}/"
	${INSTALL_FILE} etc/broker/defaults.conf "${DESTDIR}${ETCDIR}/broker/"
	${INSTALL_FILE} etc/broker/x2gobroker-sessionprofiles.conf "${DESTDIR}${ETCDIR}/broker/"
	${INSTALL_FILE} etc/broker/x2gobroker-loggers.conf "${DESTDIR}${ETCDIR}/broker/"
	if [ -e /etc/debian_version ]; then ${INSTALL_FILE} pam/x2gobroker.Debian "${DESTDIR}/etc/pam.d/x2gobroker"; fi
	if [ -e /etc/redhat-release ]; then ${INSTALL_FILE} pam/x2gobroker.RHEL "${DESTDIR}/etc/pam.d/x2gobroker"; fi
	if [ -e /etc/os-release ] && cat /etc/os-release | grep "suse" 1>/dev/null || [ -d /usr/share/doc/packages/brp-check-suse ]; then ${INSTALL_FILE} pam/x2gobroker.SUSE "${DESTDIR}/etc/pam.d/x2gobroker"; fi

	# x2gobroker-agent
	mkdir -p "${DESTDIR}${LIBDIR}" "${DESTDIR}${SBINDIR}" \
	        "${DESTDIR}${MANDIR}/man8"
	${INSTALL_FILE} man/man8/x2gobroker-pubkeyauthorizer.8* \
	        "${DESTDIR}${MANDIR}/man8"
	${INSTALL_PROGRAM} lib/x2gobroker-agent* "${DESTDIR}${LIBDIR}/"
	${INSTALL_PROGRAM} sbin/x2gobroker-pubkeyauthorizer "${DESTDIR}${SBINDIR}/"

	# x2gobroker-authservice
	mkdir -p "${DESTDIR}${SBINDIR}" "${DESTDIR}/etc/logrotate.d" \
	        "${DESTDIR}${ETCDIR}/broker" "${DESTDIR}/etc/default" \
	        "${DESTDIR}${MANDIR}/man8"
	${INSTALL_FILE} defaults/x2gobroker-authservice.default \
	        "${DESTDIR}/etc/default/x2gobroker-authservice"
	${INSTALL_FILE} man/man8/x2gobroker-authservice.8* \
	        "${DESTDIR}${MANDIR}/man8"
	${INSTALL_PROGRAM} sbin/x2gobroker-authservice "${DESTDIR}${SBINDIR}/"
	${INSTALL_FILE} logrotate/x2gobroker-authservice \
	        "${DESTDIR}/etc/logrotate.d/"
	${INSTALL_FILE} etc/broker/x2gobroker-authservice-logger.conf \
	        "${DESTDIR}${ETCDIR}/broker/"

	# x2gobroker-loadchecker
	mkdir -p "${DESTDIR}${SBINDIR}" "${DESTDIR}/etc/logrotate.d" \
	        "${DESTDIR}${ETCDIR}/broker" "${DESTDIR}/etc/default" \
	        "${DESTDIR}${MANDIR}/man8"
	${INSTALL_FILE} defaults/x2gobroker-loadchecker.default \
	        "${DESTDIR}/etc/default/x2gobroker-loadchecker"
	${INSTALL_FILE} man/man8/x2gobroker-loadchecker.8* \
	        "${DESTDIR}${MANDIR}/man8"
	${INSTALL_PROGRAM} sbin/x2gobroker-loadchecker "${DESTDIR}${SBINDIR}/"
	${INSTALL_FILE} logrotate/x2gobroker-loadchecker \
	        "${DESTDIR}/etc/logrotate.d/"
	${INSTALL_FILE} etc/broker/x2gobroker-loadchecker-logger.conf \
	        "${DESTDIR}${ETCDIR}/broker/"

	# x2gobroker-daemon
	mkdir -p "${DESTDIR}/etc/logrotate.d/" "${DESTDIR}/etc/default"
	${INSTALL_FILE} defaults/x2gobroker-daemon.default \
	        "${DESTDIR}/etc/default/x2gobroker-daemon"
	${INSTALL_FILE} logrotate/x2gobroker-daemon \
	        "${DESTDIR}/etc/logrotate.d/"
	mkdir -p "${DESTDIR}${BINDIR}" "${DESTDIR}${SBINDIR}" \
	        "${DESTDIR}${MANDIR}/man1" "${DESTDIR}${MANDIR}/man8"
	${INSTALL_FILE} man/man1/x2gobroker-daemon.1* \
	        "${DESTDIR}${MANDIR}/man1"
	${INSTALL_FILE} man/man8/x2gobroker-daemon-debug.8* \
	        "${DESTDIR}${MANDIR}/man8"
	${INSTALL_SYMLINK} x2gobroker \
	        "${DESTDIR}${BINDIR}/x2gobroker-daemon"
	${INSTALL_PROGRAM} sbin/x2gobroker-daemon-debug \
	        "${DESTDIR}${SBINDIR}/"

	# x2gobroker-ssh
	mkdir -p "${DESTDIR}${BINDIR}" "${DESTDIR}${SBINDIR}" \
	         "${DESTDIR}${ETCDIR}/../sudoers.d" \
	         "${DESTDIR}${MANDIR}/man1"
	${INSTALL_FILE} man/man1/x2gobroker-ssh.1* \
	        "${DESTDIR}${MANDIR}/man1"
	${INSTALL_PROGRAM} bin/x2gobroker-ssh \
	        "${DESTDIR}${BINDIR}/"
	${INSTALL_FILE} x2gobroker-ssh.sudo \
	        "${DESTDIR}${ETCDIR}/../sudoers.d/"
	mv "${DESTDIR}${ETCDIR}/../sudoers.d/x2gobroker-ssh.sudo" "${DESTDIR}${ETCDIR}/../sudoers.d/x2gobroker-ssh"

	# x2gobroker-wsgi
	mkdir -p "${DESTDIR}${ETCDIR}" "${DESTDIR}/etc/logrotate.d"
	${INSTALL_FILE} etc/x2gobroker-wsgi.apache.{conf,vhost} \
	        "${DESTDIR}${ETCDIR}/"
	${INSTALL_FILE} logrotate/x2gobroker-wsgi "${DESTDIR}/etc/logrotate.d/"

	# x2gobroker
	mkdir -p "${DESTDIR}${BINDIR}" "${DESTDIR}${SBINDIR}" \
	        "${DESTDIR}${MANDIR}/man1"
	mkdir -p "${DESTDIR}${BINDIR}" "${DESTDIR}${SBINDIR}" \
	        "${DESTDIR}${MANDIR}/man8"
	${INSTALL_FILE} man/man1/x2gobroker{,-testauth}.1* \
	        "${DESTDIR}${MANDIR}/man1"
	${INSTALL_FILE} man/man8/x2gobroker{-keygen,-testagent}.8* \
	        "${DESTDIR}${MANDIR}/man8"
	${INSTALL_PROGRAM} bin/x2gobroker bin/x2gobroker-testauth \
	        "${DESTDIR}${BINDIR}/"
	${INSTALL_PROGRAM} sbin/x2gobroker-keygen sbin/x2gobroker-testagent \
	        "${DESTDIR}${SBINDIR}/"

