#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-

# Copyright (C) 2012-2015 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# X2Go Session Broker is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# X2Go Session Broker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

from setuptools import setup, find_packages

import os
__VERSION__ = None
for line in file(os.path.join('x2gobroker', '__init__.py')).readlines():
    if (line.startswith('__VERSION__')):
        exec(line.strip())
__AUTHOR__ = None
for line in file(os.path.join('x2gobroker', '__init__.py')).readlines():
    if (line.startswith('__AUTHOR__')):
        exec(line.strip())
MODULE_VERSION = __VERSION__
MODULE_AUTHOR = __AUTHOR__

setup(
    name = "x2gobroker",
    version = MODULE_VERSION,
    description = "X2Go Session Broker",
    license = 'AGPLv3+',
    author = MODULE_AUTHOR,
    url = 'http://www.x2go.org',
    packages = find_packages('.'),
    package_dir = {'': '.'},
    test_suite = "x2gobroker.tests.runalltests",
    use_2to3 = True,
)
