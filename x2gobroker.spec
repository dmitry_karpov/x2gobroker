Name:           x2gobroker
Version:        0.0.3.1
Release:        0.0x2go1%{?dist}
Summary:        X2Go Session Broker
%if 0%{?suse_version}
License:        AGPL-3.0+
Group:          Productivity/Networking/Remote Desktop
%else
License:        AGPLv3+
Group:          Applications/Communications
%endif
Url:            http://www.x2go.org/

Source0:        http://code.x2go.org/releases/source/%name/%name-%version.tar.gz
Source1:        x2gobroker-daemon.init
Source2:        x2gobroker-authservice.init
Source3:        x2gobroker-loadchecker.init
Source4:        x2gobroker-rpmlintrc

%if 0%{?el5}
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
%endif

%if 0%{?suse_version}
BuildRequires:  python-devel
BuildRequires:  fdupes
%else
BuildRequires:  python2-devel
%endif
BuildRequires:  sudo
BuildRequires:  python-setuptools
%if 0%{?fedora} || 0%{?el7} || 0%{?suse_version} >= 1230
BuildRequires:  systemd
%endif
Requires:       python-argparse
Requires:       python-setproctitle
Requires:       python-wsgilog
Requires(pre):  python-x2gobroker = %{version}-%{release}

%if 0%{?suse_version}
Requires(pre): pwdutils
%else
Requires(pre):  shadow-utils
%endif

%description
X2Go is a server based computing environment with
    - session resuming
    - low bandwidth support
    - session brokerage support
    - client side mass storage mounting support
    - client side printing support
    - audio support
    - authentication by smartcard and USB stick

The session broker is a server tool for X2Go that tells your X2Go Client
application in a terminal server cluster what servers and session types are
most appropriate for the user in front of the X2Go terminal.

A session broker is most useful in load balanced X2Go server farms.

This package contains the x2gobroker executable.

%package -n python-x2gobroker
Summary:        X2Go Session Broker (Python modules)
%if 0%{?suse_version}
Group:          Productivity/Networking/Remote Desktop
%else
Group:          Applications/Communications
%endif
%if 0%{?suse_version}
Requires:       python
%else
Requires:       python2
%endif
%if 0%{?suse_version}
Requires:       python-pampy
%else
Requires:       python-pam
%endif
Requires:       python-netaddr
Requires:       python-tornado
Requires:       python-paramiko
Requires:       python-daemon
Requires:       python-lockfile
%if 0%{?suse_version}
Requires(pre):  permissions
%endif

%description -n python-x2gobroker
X2Go is a server based computing environment with
    - session resuming
    - low bandwidth support
    - session brokerage support
    - client side mass storage mounting support
    - client side printing support
    - audio support
    - authentication by smartcard and USB stick

The session broker is a server tool for X2Go that tells your X2Go Client
application in a terminal server cluster what servers and session types are
most appropriate for the user in front of the X2Go terminal.

A session broker is most useful in load balanced X2Go server farms.

This package contains the broker's Python library.


%package authservice
Summary:        X2Go Session Broker (PAM authentication service)
%if 0%{?suse_version}
Group:          Productivity/Networking/Remote Desktop
%else
Group:          Applications/Communications
%endif
%if 0%{?suse_version}
Requires:       python
%else
Requires:       python2
%endif
Requires:       python-argparse
Requires:       python-setproctitle
%if 0%{?suse_version}
Requires:       python-pampy
%else
Requires:       python-pam
%endif
Requires:       logrotate
Requires(pre):  python-x2gobroker = %{version}-%{release}
%if 0%{?suse_version}
Requires(pre):  permissions
%endif

%description authservice
X2Go is a server based computing environment with
    - session resuming
    - low bandwidth support
    - session brokerage support
    - client side mass storage mounting support
    - client side printing support
    - audio support
    - authentication by smartcard and USB stick

The session broker is a server tool for X2Go that tells your X2Go Client
application in a terminal server cluster what servers and session types are
most appropriate for the user in front of the X2Go terminal.

A session broker is most useful in load balanced X2Go server farms.

This package contains the authentication service against the PAM system.

%package loadchecker
Summary:        X2Go Session Broker (load checker service)
%if 0%{?suse_version}
Group:          Productivity/Networking/Remote Desktop
%else
Group:          Applications/Communications
%endif
%if 0%{?suse_version}
Requires:       python
%else
Requires:       python2
%endif
Requires:       python-argparse
Requires:       python-setproctitle
Requires:       logrotate
Requires(pre):  python-x2gobroker = %{version}-%{release}
%if 0%{?suse_version}
Requires(pre):  permissions
%endif

%description loadchecker
X2Go is a server based computing environment with
    - session resuming
    - low bandwidth support
    - session brokerage support
    - client side mass storage mounting support
    - client side printing support
    - audio support
    - authentication by smartcard and USB stick

The session broker is a server tool for X2Go that tells your X2Go Client
application in a terminal server cluster what servers and session types are
most appropriate for the user in front of the X2Go terminal.

A session broker is most useful in load balanced X2Go server farms.

This package contains the load checker service required for broker setups
with dynamic load balancing.


%package daemon
Summary:        X2Go Session Broker (standalone daemon)
%if 0%{?suse_version}
Group:          Productivity/Networking/Remote Desktop
%else
Group:          Applications/Communications
%endif
Requires:       x2gobroker = %{version}-%{release}
Requires:       x2gobroker-authservice = %{version}-%{release}
Requires:       logrotate
%if 0%{?suse_version}
Requires(pre):  permissions
%endif

%description daemon
X2Go is a server based computing environment with
    - session resuming
    - low bandwidth support
    - session brokerage support
    - client side mass storage mounting support
    - client side printing support
    - audio support
    - authentication by smartcard and USB stick

The session broker is a server tool for X2Go that tells your X2Go Client
application in a terminal server cluster what servers and session types are
most appropriate for the user in front of the X2Go terminal.

A session broker is most useful in load balanced X2Go server farms.

This package contains the start-stop script that installs the X2Go Session Broker
as standalone daemon.


%package ssh
Summary:        X2Go Session Broker (SSH broker)
%if 0%{?suse_version}
Group:          Productivity/Networking/Remote Desktop
%else
Group:          Applications/Communications
%endif
Requires:       x2gobroker = %{version}-%{release}
%if 0%{?suse_version}
Requires(pre):  permissions
%endif

%description ssh
X2Go is a server based computing environment with
    - session resuming
    - low bandwidth support
    - session brokerage support
    - client side mass storage mounting support
    - client side printing support
    - audio support
    - authentication by smartcard and USB stick

The session broker is a server tool for X2Go that tells your X2Go Client
application in a terminal server cluster what servers and session types are
most appropriate for the user in front of the X2Go terminal.

A session broker is most useful in load balanced X2Go server farms.

This add-on package provides fully-featured SSH brokerage support (with access
to broker agents on remote X2Go servers).


%package wsgi
Summary:        X2Go Session Broker (WSGI)
%if 0%{?suse_version}
Group:          Productivity/Networking/Remote Desktop
%else
Group:          Applications/Communications
%endif
Requires:       x2gobroker = %{version}-%{release}
%if 0%{?suse_version}
Requires:       apache2, apache2-mod_wsgi
%else
Requires:       httpd, mod_wsgi
%endif
%if 0%{?suse_version}
Requires:       python
%else
Requires:       python2
%endif
Requires:       x2gobroker-authservice = %{version}-%{release}
Requires:       logrotate
%if 0%{?suse_version}
Requires(pre):  permissions
%endif

%description wsgi
X2Go is a server based computing environment with
    - session resuming
    - low bandwidth support
    - session brokerage support
    - client side mass storage mounting support
    - client side printing support
    - audio support
    - authentication by smartcard and USB stick

The session broker is a server tool for X2Go that tells your X2Go Client
application in a terminal server cluster what servers and session types are
most appropriate for the user in front of the X2Go terminal.

A session broker is most useful in load balanced X2Go server farms.

This package contains an Apache2 configuration that installs the X2Go Session
Broker as a WSGI application into a running Apache2 httpd.

%package agent
Summary:        X2Go Session Broker (remote agent)
%if 0%{?suse_version}
Group:          Productivity/Networking/Remote Desktop
%else
Group:          Applications/Communications
%endif
%if 0%{?suse_version}
%if 0%{?suse_version} < 1140
Requires:       perl = %{perl_version}
%else
%{perl_requires}
%endif
%else
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
%endif
%if 0%{?suse_version}
Requires:       python
%else
Requires:       python2
%endif
Requires:       python-setproctitle
Requires:       python-argparse
Requires:       python-paramiko
Requires:       perl(File::Which)
%if 0%{?suse_version}
Requires(pre):  permissions
%endif

%description agent
X2Go is a server based computing environment with
    - session resuming
    - low bandwidth support
    - session brokerage support
    - client side mass storage mounting support
    - client side printing support
    - audio support
    - authentication by smartcard and USB stick

The session broker is a server tool for X2Go that tells your X2Go Client
application in a terminal server cluster what servers and session types are
most appropriate for the user in front of the X2Go terminal.

A session broker is most useful in load balanced X2Go server farms.

This package contains a setuid agent command that extends X2Go Session Broker
functionality. It has to be installed on X2Go Servers that shall be
controlled via a session broker.

The broker agent provides means to the X2Go Session Broker for controlling
the X2Go Server it is installed on (e.g. suspend/terminate sessions, deploy
SSH login keys, detect server load, detect running/suspended sessions
of connecting users, etc.).

WARNING: This package installs a setuid wrapper
(%{_libdir}/x2go/broker/x2gobroker-agent) on your system. This setuid wrapper
aims to be a secure replacement for the deprecated suidperl exectuable that
was removed from Perl (>= 5.12).

This wrapper is only able to execute the Perl script
%{_libdir}/x2go/broker/x2gobroker-agent.pl. For running properly,
x2gobroker-agent.pl needs setuid root privileges.

If you hesitate to install this package, study the code of the named wrapper
and the named Perl script beforehand. Note that the X2Go session broker will
lack functionality, but it will work without this x2gobroker-agent component
installed on your to-be-managed X2Go servers.


%prep
%setup -q

%build
echo "Files where we will be patching libexecedir:"
find . -type f -exec grep -l "/usr/lib/x2go/" "{}" "+"
find . -type f -exec grep -l "/usr/lib/x2go/" "{}" "+" | \
	xargs perl -i -pe 's{/usr/lib/x2go/}{%_libexecdir/x2go/}'
%if 0%{?el5} || 0%{?el6}
sed -i etc/broker/x2gobroker-sessionprofiles.conf \
    -e 's/localhost-mate/localhost-gnome/' \
    -e 's/^name=MATE - localhost/name=GNOME - localhost/' \
    -e 's/^command=MATE/command=GNOME/'
%endif
%if 0%{?fedora} >= 22
grep -l -r -E '^#!/usr/bin/env python$' | while read file; do \
    sed -i "$file" \
        -e 's#/usr/bin/env python#/usr/bin/env python2#'
done
%endif
sed -i logrotate/x2gobroker-authservice \
    -e 's/adm/root/'
sed -i logrotate/x2gobroker-loadchecker \
    -e 's/adm/root/'
sed -i logrotate/x2gobroker-daemon \
    -e 's/adm/root/'
sed -i logrotate/x2gobroker-wsgi \
    -e 's/adm/root/'
make %{?_smp_mflags} PREFIX="%_prefix" LIBDIR="%_libexecdir/x2go"

%install
b="%buildroot"
make install PREFIX="%_prefix" LIBDIR="%_libexecdir/x2go" DESTDIR="$b"

#
# Somewhat distro specific (especially the paths), thus this was not
# done in the shipped tarball's Makefile
#
%if 0%{?suse_version}
mkdir -p "$b/%_sysconfdir/apache2"/{conf.d,vhosts.d}
ln -s "%_sysconfdir/x2go/x2gobroker-wsgi.apache.conf" \
	"$b/%_sysconfdir/apache2/conf.d/x2gobroker-wsgi.conf"
ln -s "%_sysconfdir/x2go/x2gobroker-wsgi.apache.vhost" \
	"$b/%_sysconfdir/apache2/vhosts.d/x2gobroker-wsgi.sample"
%else
mkdir -p "$b/%_sysconfdir/httpd"/{conf.d,vhosts.d}
ln -s "%_sysconfdir/x2go/x2gobroker-wsgi.apache.conf" \
	"$b/%_sysconfdir/httpd/conf.d/x2gobroker-wsgi.conf"
ln -s "%_sysconfdir/x2go/x2gobroker-wsgi.apache.vhost" \
	"$b/%_sysconfdir/httpd/vhosts.d/x2gobroker-wsgi.sample"
%endif

%if 0%{?fedora} || 0%{?el7} || 0%{?suse_version} >= 1230
# System.d session cleanup script
mkdir -p %{buildroot}%{_unitdir}
install -pm0644 x2gobroker-daemon.service %{buildroot}%{_unitdir}
install -pm0644 x2gobroker-authservice.service %{buildroot}%{_unitdir}
install -pm0644 x2gobroker-loadchecker.service %{buildroot}%{_unitdir}
rm -f %{buildroot}%{_sysconfdir}/default/x2gobroker-daemon
rm -f %{buildroot}%{_sysconfdir}/default/x2gobroker-authservice
rm -f %{buildroot}%{_sysconfdir}/default/x2gobroker-loadchecker
rm -f %{buildroot}%{_sysconfdir}/default/python-x2gobroker
%else
# SysV session cleanup script
%if 0%{?el5}
rm -f %{buildroot}%{_sysconfdir}/x2go/broker/defaults.conf
mkdir -p %{buildroot}%{_initrddir}
install -pm0755 %SOURCE3 \
	"$b/%_initrddir/x2gobroker-loadchecker"
install -pm0755 %SOURCE2 \
	"$b/%_initrddir/x2gobroker-authservice"
install -pm0755 %SOURCE1 \
	"$b/%_initrddir/x2gobroker-daemon"
%endif
%if 0%{?el6} || ( 0%{?suse_version} && 0%{?suse_version} < 1140)
rm -f %{buildroot}%{_sysconfdir}/x2go/broker/defaults.conf
mkdir -p "$b/%_initddir"
install -pm0755 %SOURCE3 \
	"$b/%_initddir/x2gobroker-loadchecker"
install -pm0755 %SOURCE2 \
	"$b/%_initddir/x2gobroker-authservice"
install -pm0755 %SOURCE1 \
	"$b/%_initddir/x2gobroker-daemon"
%endif
%endif

#
# Totally distro-specific
#
%if 0%{?fdupes:1}
%fdupes %buildroot/%_prefix
%endif


%pre -n python-x2gobroker
if ! /usr/bin/getent group x2gobroker 1>/dev/null 2>/dev/null && /usr/sbin/groupadd -r x2gobroker; then
	if ! /usr/bin/getent passwd x2gobroker 1>/dev/null 2>/dev/null; then
		/usr/sbin/useradd -c "X2Go Broker System User" \
		    -d "%_localstatedir/lib/x2gobroker" \
		    -g x2gobroker -r -s /bin/bash x2gobroker || :
	fi
fi

%if 0%{?suse_version}
%post -n python-x2gobroker
%set_permissions %{_localstatedir}/log/x2gobroker


%verifyscript -n python-x2gobroker
%verify_permissions -e %{_localstatedir}/log/x2gobroker
%endif


%if 0%{?suse_version} >= 1230
%pre authservice
%service_add_pre x2gobroker-authservice.service
%endif

%post authservice
%if 0%{?fedora} || 0%{?el7} || 0%{?suse_version} >= 1230
%if 0%{?suse_version}
%service_add_post x2gobroker-authservice.service
%else
%systemd_post x2gobroker-authservice.service
%endif
%else
/sbin/chkconfig --add x2gobroker-authservice
if [ "$1" -ge "1" ] ; then
    /sbin/service x2gobroker-authservice condrestart >/dev/null 2>&1 || :
fi
%endif
%if 0%{?suse_version}
%set_permissions %{_localstatedir}/log/x2gobroker


%verifyscript authservice
%verify_permissions -e %{_localstatedir}/log/x2gobroker
%endif

%preun authservice
%if 0%{?fedora} || 0%{?el7} || 0%{?suse_version} >= 1230
%if 0%{?suse_version}
%service_del_preun x2gobroker-authservice.service
%else
%systemd_preun x2gobroker-authservice.service
%endif
%else
if [ "$1" = 0 ]; then
        /sbin/service x2gobroker-authservice stop >/dev/null 2>&1
        /sbin/chkconfig --del x2gobroker-authservice
fi
%endif

%if 0%{?fedora} || 0%{?el7} || 0%{?suse_version} >= 1230
%postun authservice
%if 0%{?suse_version}
%service_del_postun x2gobroker-authservice.service
%else
%systemd_postun x2gobroker-authservice.service
%endif
%endif


%if 0%{?suse_version} >= 1230
%pre loadchecker
%service_add_pre x2gobroker-loadchecker.service
%endif

%post loadchecker
%if 0%{?fedora} || 0%{?el7} || 0%{?suse_version} >= 1230
%if 0%{?suse_version}
%service_add_post x2gobroker-loadchecker.service
%else
%systemd_post x2gobroker-loadchecker.service
%endif
%else
/sbin/chkconfig --add x2gobroker-loadchecker
if [ "$1" -ge "1" ] ; then
    /sbin/service x2gobroker-loadchecker condrestart >/dev/null 2>&1 || :
fi
%endif
%if 0%{?suse_version}
%set_permissions %{_localstatedir}/log/x2gobroker


%verifyscript loadchecker
%verify_permissions -e %{_localstatedir}/log/x2gobroker
%endif

%preun loadchecker
%if 0%{?fedora} || 0%{?el7} || 0%{?suse_version} >= 1230
%if 0%{?suse_version}
%service_del_preun x2gobroker-loadchecker.service
%else
%systemd_preun x2gobroker-loadchecker.service
%endif
%else
if [ "$1" = 0 ]; then
        /sbin/service x2gobroker-loadchecker stop >/dev/null 2>&1
        /sbin/chkconfig --del x2gobroker-loadchecker
fi
%endif

%if 0%{?fedora} || 0%{?el7} || 0%{?suse_version} >= 1230
%postun loadchecker
%if 0%{?suse_version}
%service_del_postun x2gobroker-loadchecker.service
%else
%systemd_postun x2gobroker-loadchecker.service
%endif
%endif


%if 0%{?suse_version} >= 1230
%pre daemon
%service_add_pre x2gobroker-daemon.service
%endif

%post daemon
%if 0%{?fedora} || 0%{?el7} || 0%{?suse_version} >= 1230
%if 0%{?suse_version}
%service_add_post x2gobroker-daemon.service
%else
%systemd_post x2gobroker-daemon.service
%endif
%else
/sbin/chkconfig --add x2gobroker-daemon
if [ "$1" -ge "1" ] ; then
    /sbin/service x2gobroker-daemon condrestart >/dev/null 2>&1 || :
fi
%endif
%if 0%{?suse_version}
%set_permissions %{_localstatedir}/log/x2gobroker


%verifyscript daemon
%verify_permissions -e %{_localstatedir}/log/x2gobroker
%endif

%preun daemon
%if 0%{?fedora} || 0%{?el7} || 0%{?suse_version} >= 1230
%if 0%{?suse_version}
%service_del_preun x2gobroker-daemon.service
%else
%systemd_preun x2gobroker-daemon.service
%endif
%else
if [ "$1" = 0 ]; then
        /sbin/service x2gobroker-daemon stop >/dev/null 2>&1
        /sbin/chkconfig --del x2gobroker-daemon
fi
%endif

%if 0%{?fedora} || 0%{?el7} || 0%{?suse_version} >= 1230
%postun daemon
%if 0%{?suse_version}
%service_del_postun x2gobroker-daemon.service
%else
%systemd_postun x2gobroker-daemon.service
%endif
%endif


%post ssh
if ! /usr/bin/getent group x2gobroker-users 1>/dev/null 2>/dev/null; then
	/usr/sbin/groupadd -r x2gobroker-users
fi
%if 0%{?suse_version}
%set_permissions /usr/bin/x2gobroker-ssh


%verifyscript ssh
%verify_permissions -e /usr/bin/x2gobroker-ssh
%endif

%pre agent
if ! /usr/bin/getent group x2gobroker 1>/dev/null 2>/dev/null && /usr/sbin/groupadd -r x2gobroker; then
	if ! /usr/bin/getent passwd x2gobroker 1>/dev/null 2>/dev/null; then
		/usr/sbin/useradd -c "X2Go Broker System User" \
		    -d "%_localstatedir/lib/x2gobroker" \
		    -g x2gobroker -r -s /bin/bash x2gobroker || :
	fi
fi


%if 0%{?suse_version}
%post agent
%set_permissions %{_libdir}/x2go/x2gobroker-agent
%set_permissions %{_localstatedir}/log/x2gobroker


%verifyscript agent
%verify_permissions -e %{_libdir}/x2go/x2gobroker-agent
%verify_permissions -e %{_localstatedir}/log/x2gobroker
%endif


%if 0%{?suse_version}
%post wsgi
%set_permissions %{_localstatedir}/log/x2gobroker


%verifyscript wsgi
%verify_permissions -e %{_localstatedir}/log/x2gobroker
%endif


%files
%defattr(-,root,root)
%_bindir/x2gobroker
%_bindir/x2gobroker-testauth
%_sbindir/x2gobroker-keygen
%_sbindir/x2gobroker-testagent
%_mandir/man1/x2gobroker*.1*
%exclude %_mandir/man1/x2gobroker-ssh.1*
%exclude %_mandir/man1/x2gobroker-daemon.1*
%_mandir/man8/x2gobroker-keygen.8*
%_mandir/man8/x2gobroker-testagent.8*


%files -n python-x2gobroker
%defattr(-,root,root)
%config(noreplace) %_sysconfdir/x2go
%exclude %_sysconfdir/x2go/x2gobroker-wsgi.apache.conf
%exclude %_sysconfdir/x2go/x2gobroker-wsgi.apache.vhost
%config %_sysconfdir/pam.d
%if 0%{?el5} || 0%{?el6} || (0%{?suse_version} && 0%{?suse_version} < 1140)
%config %_sysconfdir/default/python-x2gobroker
%endif
%python_sitelib/x2gobroker*
%attr(02750,x2gobroker,x2gobroker) %_localstatedir/log/x2gobroker
%attr(00750,x2gobroker,x2gobroker) %_localstatedir/lib/x2gobroker


%files authservice
%defattr(-,root,root)
%if 0%{?el5}
%_initrddir/x2gobroker-authservice
%endif
%if 0%{?el6}
%_initddir/x2gobroker-authservice
%endif
%if 0%{?fedora} || 0%{?el7} || 0%{?suse_version} >= 1230
%{_unitdir}/x2gobroker-authservice.service
%endif
%if 0%{?el5} || 0%{?el6} || (0%{?suse_version} && 0%{?suse_version} < 1140)
%config %_sysconfdir/default/x2gobroker-authservice
%endif
%config %_sysconfdir/logrotate.d/x2gobroker-authservice
%_sbindir/x2gobroker-authservice
%_mandir/man8/x2gobroker-authservice.8*
%attr(02750,x2gobroker,x2gobroker) %_localstatedir/log/x2gobroker


%files loadchecker
%defattr(-,root,root)
%if 0%{?el5}
%_initrddir/x2gobroker-loadchecker
%endif
%if 0%{?el6}
%_initddir/x2gobroker-loadchecker
%endif
%if 0%{?fedora} || 0%{?el7} || 0%{?suse_version} >= 1230
%{_unitdir}/x2gobroker-loadchecker.service
%endif
%if 0%{?el5} || 0%{?el6} || (0%{?suse_version} && 0%{?suse_version} < 1140)
%config %_sysconfdir/default/x2gobroker-loadchecker
%endif
%config %_sysconfdir/logrotate.d/x2gobroker-loadchecker
%_sbindir/x2gobroker-loadchecker
%_mandir/man8/x2gobroker-loadchecker.8*
%attr(02750,x2gobroker,x2gobroker) %_localstatedir/log/x2gobroker


%files daemon
%defattr(-,root,root)
%_bindir/x2gobroker-daemon
%if 0%{?el5}
%_initrddir/x2gobroker-daemon
%endif
%if 0%{?el6}
%_initddir/x2gobroker-daemon
%endif
%if 0%{?fedora} || 0%{?el7} || 0%{?suse_version} >= 1230
%{_unitdir}/x2gobroker-daemon.service
%endif
%if 0%{?el5} || 0%{?el6} || (0%{?suse_version} && 0%{?suse_version} < 1140)
%config %_sysconfdir/default/x2gobroker-daemon
%endif
%_sbindir/x2gobroker-daemon-debug
%_mandir/man1/x2gobroker-daemon.1*
%_mandir/man8/x2gobroker-daemon-debug.8*
%config %_sysconfdir/logrotate.d/x2gobroker-daemon
%attr(02750,x2gobroker,x2gobroker) %_localstatedir/log/x2gobroker


%files ssh
%defattr(-,root,root)
%attr(04550,x2gobroker,x2gobroker-users) %_bindir/x2gobroker-ssh
%_mandir/man1/x2gobroker-ssh.1*
%_sysconfdir/sudoers.d/x2gobroker-ssh


%files wsgi
%defattr(-,root,root)
%if 0%{?suse_version}
%_sysconfdir/apache2
%else
%_sysconfdir/httpd
%endif
%attr(02750,x2gobroker,x2gobroker) %_localstatedir/log/x2gobroker
%config %_sysconfdir/x2go/x2gobroker-wsgi.apache.conf
%config %_sysconfdir/x2go/x2gobroker-wsgi.apache.vhost
%config %_sysconfdir/logrotate.d/x2gobroker-wsgi


%files agent
%defattr(-,root,root)
%attr(02750,x2gobroker,x2gobroker) %_localstatedir/log/x2gobroker
%attr(00750,x2gobroker,x2gobroker) %_localstatedir/lib/x2gobroker
%dir %_libexecdir/x2go
%attr(04750,root,x2gobroker) %_libexecdir/x2go/x2gobroker-agent
%_libexecdir/x2go/x2gobroker-agent.pl
%_sbindir/x2gobroker-pubkeyauthorizer
%_mandir/man8/x2gobroker-pubkeyauthorizer.8*


%changelog
