# -*- coding: utf-8 -*-

# Copyright (C) 2012-2015 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# X2Go Session Broker is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# X2Go Session Broker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import unittest

# Python X2GoBroker modules
import x2gobroker.utils

class TestX2GoBrokerUtils(unittest.TestCase):

    ### TEST FUNCTION: normalize_hostnames() with server lists (ListType)

    def test_normalize_hostnames_listtype(self):

        server_list = ['ts01', 'ts02', 'ts03',]
        expected_server_list = ['ts01', 'ts02', 'ts03',]
        server_list_n, subdomains = x2gobroker.utils.normalize_hostnames(server_list)
        server_list_n.sort()
        self.assertEqual(expected_server_list, server_list_n)
        self.assertEqual([], subdomains)

        server_list = ['ts01.intern', 'ts02.intern', 'ts03.intern',]
        expected_server_list = ['ts01', 'ts02', 'ts03',]
        server_list_n, subdomains = x2gobroker.utils.normalize_hostnames(server_list)
        server_list_n.sort()
        self.assertEqual(expected_server_list, server_list_n)
        self.assertEqual(['intern'], subdomains)

        server_list = ['ts01.intern', 'ts02.intern', 'ts03.extern',]
        expected_server_list = ['ts01.intern', 'ts02.intern', 'ts03.extern',]
        server_list_n, subdomains = x2gobroker.utils.normalize_hostnames(server_list)
        server_list_n.sort()
        subdomains.sort()
        self.assertEqual(expected_server_list, server_list_n)
        self.assertEqual(['extern', 'intern'], subdomains)

        server_tuple = ('ts01.intern', 'ts02.intern', 'ts03.extern',)
        expected_server_list = ['ts01.intern', 'ts02.intern', 'ts03.extern',]
        server_list_n, subdomains = x2gobroker.utils.normalize_hostnames(server_tuple)
        server_list_n.sort()
        subdomains.sort()
        self.assertEqual(expected_server_list, server_list_n)
        self.assertEqual(['extern', 'intern'], subdomains)

    ### TEST FUNCTION: normalize_hostnames() with server load (DictType)

    def test_normalize_hostnames_dicttype(self):

        server_load = {'ts01': 10, 'ts02': 20, 'ts03': 30,}
        expected_server_load = {'ts01': 10, 'ts02': 20, 'ts03': 30,}
        server_load_n, subdomains = x2gobroker.utils.normalize_hostnames(server_load)
        sln_keys = server_load_n.keys()
        esl_keys = expected_server_load.keys()
        sln_keys.sort()
        esl_keys.sort()
        self.assertEqual(esl_keys, sln_keys)
        self.assertEqual([], subdomains)

        server_load = {'ts01.intern': 10, 'ts02.intern': 20, 'ts03.intern': 30,}
        expected_server_load = {'ts01': 10, 'ts02': 20, 'ts03': 30,}
        server_load_n, subdomains = x2gobroker.utils.normalize_hostnames(server_load)
        sln_keys = server_load_n.keys()
        esl_keys = expected_server_load.keys()
        sln_keys.sort()
        esl_keys.sort()
        self.assertEqual(esl_keys, sln_keys)
        self.assertEqual(['intern'], subdomains)

        server_load = {'ts01.intern': 10, 'ts02.intern': 20, 'ts03.extern': 30,}
        expected_server_load = {'ts01.intern': 10, 'ts02.intern': 20, 'ts03.extern': 30,}
        server_load_n, subdomains = x2gobroker.utils.normalize_hostnames(server_load)
        sln_keys = server_load_n.keys()
        esl_keys = expected_server_load.keys()
        sln_keys.sort()
        esl_keys.sort()
        subdomains.sort()
        self.assertEqual(esl_keys, sln_keys)
        self.assertEqual(['extern', 'intern'], subdomains)

    def test_matching_hostnames(self):

        server_list_a = ['server1', 'server2', 'server3']
        server_list_b = ['server2', 'server3', 'server4']
        matching_hostnames = x2gobroker.utils.matching_hostnames(server_list_a, server_list_b)
        self.assertEqual(matching_hostnames, ['server2', 'server3'])

        server_list_a = ['server1.domain1', 'server2.domain1', 'server3.domain1']
        server_list_b = ['server2', 'server3', 'server4']
        matching_hostnames = x2gobroker.utils.matching_hostnames(server_list_a, server_list_b)
        self.assertEqual(matching_hostnames, ['server2', 'server3'])

        server_list_a = ['server1', 'server2', 'server3']
        server_list_b = ['server2.domain2', 'server3.domain2', 'server4.domain2']
        matching_hostnames = x2gobroker.utils.matching_hostnames(server_list_a, server_list_b)
        self.assertEqual(matching_hostnames, ['server2', 'server3'])

        server_list_a = ['server1.domain1', 'server2.domain1', 'server3.domain1']
        server_list_b = ['server2.domain2', 'server3.domain2', 'server4.domain2']
        matching_hostnames = x2gobroker.utils.matching_hostnames(server_list_a, server_list_b)
        self.assertEqual(matching_hostnames, [])

        server_list_a = ['server1.domain1', 'server2.domain2', 'server3.domain1']
        server_list_b = ['server2.domain2', 'server3.domain2', 'server4.domain2']
        matching_hostnames = x2gobroker.utils.matching_hostnames(server_list_a, server_list_b)
        self.assertEqual(matching_hostnames, ['server2.domain2'])

        server_list_a = ['server1.domain1', 'server2', 'server3.domain1']
        server_list_b = ['server2.domain2', 'server3.domain2', 'server4.domain2']
        matching_hostnames = x2gobroker.utils.matching_hostnames(server_list_a, server_list_b)
        self.assertEqual(matching_hostnames, [])

        server_list_a = ['server1.domain1', 'server2', 'server3.domain1']
        server_list_b = ['server2', 'server3.domain2', 'server4.domain2']
        matching_hostnames = x2gobroker.utils.matching_hostnames(server_list_a, server_list_b)
        self.assertEqual(matching_hostnames, ['server2'])

    def test_split_host_address(self):

        host = 8080
        default_address = '127.0.0.1'
        default_port = 22
        bind_address, bind_port = x2gobroker.utils.split_host_address(host, default_address, default_port)
        self.assertEqual((bind_address, bind_port), (default_address, host))

        host = '8080'
        default_address = None
        default_port = None
        bind_address, bind_port = x2gobroker.utils.split_host_address(host, default_address, default_port)
        self.assertEqual((bind_address, bind_port), ('0.0.0.0', int(host)))

        host = '0.0.0.0'
        default_address = '127.0.0.1'
        default_port = 8080
        bind_address, bind_port = x2gobroker.utils.split_host_address(host, default_address, default_port)
        self.assertEqual((bind_address, bind_port), (host, default_port))

        host = '[::1]:8221'
        default_address = '127.0.0.1'
        default_port = 8080
        bind_address, bind_port = x2gobroker.utils.split_host_address(host, default_address, default_port)
        self.assertEqual((bind_address, bind_port), ('[::1]', 8221))

        host = '[::1]'
        default_address = '127.0.0.1'
        default_port = 8080
        bind_address, bind_port = x2gobroker.utils.split_host_address(host, default_address, default_port)
        self.assertEqual((bind_address, bind_port), (host, default_port))

        host = ''
        default_address = ''
        default_port = 8080
        bind_address, bind_port = x2gobroker.utils.split_host_address(host, default_address, default_port)
        self.assertEqual((bind_address, bind_port), ('0.0.0.0', default_port))

def test_suite():
    from unittest import TestSuite, makeSuite
    suite = TestSuite()
    suite.addTest(makeSuite(TestX2GoBrokerUtils))
    return suite
