# -*- coding: utf-8 -*-

# Copyright (C) 2012-2015 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# X2Go Session Broker is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# X2Go Session Broker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import unittest
import tempfile
from paste.fixture import TestApp
from nose.tools import assert_equal
import tornado.wsgi

# Python X2GoBroker modules
import x2gobroker.defaults
import x2gobroker.web.plain

urls = ( ('/plain/(.*)', x2gobroker.web.plain.X2GoBrokerWeb,) ,)
application = tornado.wsgi.WSGIApplication(urls)

class TestX2GoBrokerWebPlainBase(unittest.TestCase):

    ### TEST RESPONSE: is enabled?

    def test_isenabled(self):
        _config = """
[broker_base]
enable = false
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        _cf_bak = x2gobroker.defaults.X2GOBROKER_CONFIG
        x2gobroker.defaults.X2GOBROKER_CONFIG = tf.name
        testApp = TestApp(application)
        r = testApp.get('/plain/base/', expect_errors=True)
        assert_equal(r.status, 404)
        tf.close()
        _config = """
[broker_base]
enable = true
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        x2gobroker.defaults.X2GOBROKER_CONFIG = tf.name
        testApp = TestApp(application)
        r = testApp.get('/plain/base/', expect_errors=True)
        assert_equal(r.status, 401)
        tf.close()
        x2gobroker.defaults.X2GOBROKER_CONFIG = _cf_bak

    ### TEST RESPONSE: simple authentication (check_access)

    def test_checkaccess(self):
        testApp = TestApp(application)
        r = testApp.get('/plain/base/', expect_errors=True)
        assert_equal(r.status, 404)
        _config = """
[broker_base]
enable = true
auth-mech = testsuite
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        _cf_bak = x2gobroker.defaults.X2GOBROKER_CONFIG
        x2gobroker.defaults.X2GOBROKER_CONFIG = tf.name
        r = testApp.get('/plain/base/', params={'user': 'test', 'password': 'sweet', }, expect_errors=True)
        assert_equal(r.status, 200)
        r.mustcontain('Access granted')
        x2gobroker.defaults.X2GOBROKER_CONFIG = _cf_bak

    ### TEST TASK: listsessions (nothing should be returned for the base backend)

    def test_listsessions(self):
        _config = """
[broker_base]
enable = true
auth-mech = testsuite
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        _cf_bak = x2gobroker.defaults.X2GOBROKER_CONFIG
        x2gobroker.defaults.X2GOBROKER_CONFIG = tf.name
        testApp = TestApp(application)
        r = testApp.get('/plain/base/', params={'user': 'test', 'password': 'sweet',  'task': 'listsessions', }, expect_errors=True)
        assert_equal(r.status, 200)
        r.mustcontain('Access granted')
        r.mustcontain(no='START_USER_SESSIONS')
        r.mustcontain(no='END_USER_SESSIONS')
        r.mustcontain(no='<BR>',)
        r.mustcontain(no='<br>',)
        r.mustcontain(no='<BR />', )
        r.mustcontain(no='<br />', )
        x2gobroker.defaults.X2GOBROKER_CONFIG = _cf_bak



def test_suite():
    from unittest import TestSuite, makeSuite
    suite = TestSuite()
    suite.addTest(makeSuite(TestX2GoBrokerWebPlainBase))
    return suite
