
# Copyright (C) 2012-2015 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# X2Go Session Broker is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# X2Go Session Broker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import unittest
import tempfile
import copy

# Python X2GoBroker modules
import x2gobroker.brokers.base_broker as base
import x2gobroker.defaults

class TestX2GoBrokerBackendBase(unittest.TestCase):

    def _init_base_backend(self):
        _config_defaults = copy.deepcopy(x2gobroker.defaults.X2GOBROKER_CONFIG_DEFAULTS)
        _config_defaults.update({'broker_base': {'enable': True, }, })
        _config = "" # use the config that derives directly from the config defaults
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        return base.X2GoBroker(config_file=tf.name, config_defaults=_config_defaults)

    ### TEST CONFIGURATION: <backend> >> enable = true|false

    def test_is_enabled(self):
        _config_defaults = copy.deepcopy(x2gobroker.defaults.X2GOBROKER_CONFIG_DEFAULTS)
        _config_defaults.update({'broker_base': {'enable': True, }, })
        _config = """
[broker_base]
enable = false
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name, config_defaults=_config_defaults)
        self.assertEqual(base_backend.is_enabled(), False)
        _config = """
[broker_base]
enable = true
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name)
        self.assertEqual(base_backend.is_enabled(), True)
        tf.close()

    ### TEST CONFIGURATION: authentication mechanism (default-auth-mech vs. auth-mech in backend config)

    def test_getauthenticationmechanism(self):
        _config_defaults = copy.deepcopy(x2gobroker.defaults.X2GOBROKER_CONFIG_DEFAULTS)
        _config_defaults.update({'broker_base': {'enable': True, }, })
        _config = """
[global]
default-auth-mech = foo-auth-mech

[broker_base]
enable = true
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name)
        self.assertEqual(base_backend.get_authentication_mechanism(), 'foo-auth-mech')
        _config = """
[global]
default-auth-mech = foo-auth-mech

[broker_base]
enable = true
auth-mech = bar-auth-mech
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name, config_defaults=_config_defaults)
        self.assertEqual(base_backend.get_authentication_mechanism(), 'bar-auth-mech')
        tf.close()

    ### TEST CONFIGURATION: user DB backend (default-user-db vs. user-db in backend config)

    def test_getuserdbservice(self):
        _config_defaults = copy.deepcopy(x2gobroker.defaults.X2GOBROKER_CONFIG_DEFAULTS)
        _config_defaults.update({'broker_base': {'enable': True, }, })
        _config = """
[global]
default-user-db = foo-user-db

[broker_base]
enable = true
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name)
        self.assertEqual(base_backend.get_userdb_service(), 'foo-user-db')
        _config = """
[global]
default-user-db = foo-user-db

[broker_base]
enable = true
user-db = bar-user-db
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name, config_defaults=_config_defaults)
        self.assertEqual(base_backend.get_userdb_service(), 'bar-user-db')
        tf.close()

    ### TEST CONFIGURATION: group DB backend (default-group-db vs. group-db in backend config)

    def test_getgroupdbservice(self):
        _config_defaults = copy.deepcopy(x2gobroker.defaults.X2GOBROKER_CONFIG_DEFAULTS)
        _config_defaults.update({'broker_base': {'enable': True, }, })
        _config = """
[global]
default-group-db = foo-group-db

[broker_base]
enable = true
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name)
        self.assertEqual(base_backend.get_groupdb_service(), 'foo-group-db')
        _config = """
[global]
default-group-db = foo-group-db

[broker_base]
enable = true
group-db = bar-group-db
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name, config_defaults=_config_defaults)
        self.assertEqual(base_backend.get_groupdb_service(), 'bar-group-db')
        tf.close()

    def test_nameservicebase(self):
        _config_defaults = copy.deepcopy(x2gobroker.defaults.X2GOBROKER_CONFIG_DEFAULTS)
        _config_defaults.update({'broker_base': {'enable': True, }, })
        _config = """
[global]
default-user-db = base
default-group-db = base

[broker_base]
enable = true
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name)
        self.assertEqual(base_backend.get_users(), [])
        self.assertEqual(base_backend.has_user('any-user'), False)
        self.assertEqual(base_backend.get_groups(), [])
        self.assertEqual(base_backend.has_group('any-group'), False)
        self.assertEqual(base_backend.is_group_member('any-user', 'any-group'), False)
        self.assertEqual(base_backend.get_group_members('any-group'), [])

    def test_nameservicelibnss(self):
        _config_defaults = copy.deepcopy(x2gobroker.defaults.X2GOBROKER_CONFIG_DEFAULTS)
        _config_defaults.update({'broker_base': {'enable': True, }, })
        _config = """
[global]
default-user-db = libnss
default-group-db = libnss

[broker_base]
enable = true
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name)
        self.assertTrue( ( 'root' in base_backend.get_users() ) )
        self.assertEqual(base_backend.has_user('root'), True)
        self.assertTrue( ( 'root' in base_backend.get_groups() ) )
        self.assertEqual(base_backend.has_group('root'), True)
        self.assertEqual(base_backend.is_group_member('root', 'root'), False)
        self.assertEqual(base_backend.is_group_member('root', 'root', primary_groups=True), True)
        self.assertTrue( ( 'root' not in base_backend.get_group_members('root') ) )
        self.assertTrue( ( 'root' in base_backend.get_group_members('root', primary_groups=True) ) )

    def test_nameservicelibnss_primgroup(self):
        _config_defaults = copy.deepcopy(x2gobroker.defaults.X2GOBROKER_CONFIG_DEFAULTS)
        _config_defaults.update({'broker_base': {'enable': True, }, })
        _config = """
[global]
default-user-db = libnss
default-group-db = libnss

[broker_base]
enable = true
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name)
        self.assertTrue(type(base_backend.get_primary_group('root') in (type(''), type(u''))))
        self.assertTrue(type(unicode(base_backend.get_primary_group('root')) == u'root'))

    def test_nameservice_nodefaultsinconfig(self):
        _config_defaults = copy.deepcopy(x2gobroker.defaults.X2GOBROKER_CONFIG_DEFAULTS)
        _config = """
[global]
default-user-db =
default-group-db =

[broker_base]
enable = true
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name, config_defaults=_config_defaults)
        self.assertEqual(base_backend.get_userdb_service(), 'libnss')
        self.assertEqual(base_backend.get_groupdb_service(), 'libnss')
        _config_defaults = copy.deepcopy(x2gobroker.defaults.X2GOBROKER_CONFIG_DEFAULTS)
        _config = """
[global]
default-user-db = testsuite
default-group-db = testsuite

[broker_base]
enable = true
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name, config_defaults=_config_defaults)
        self.assertEqual(base_backend.get_userdb_service(), 'testsuite')
        self.assertEqual(base_backend.get_groupdb_service(), 'testsuite')

    ### TEST CONFIGURATION: global >> check-credentials = false

    def test_check_access_nocreds(self):
        _config_defaults = copy.deepcopy(x2gobroker.defaults.X2GOBROKER_CONFIG_DEFAULTS)
        _config_defaults.update({'broker_base': {'enable': True, }, })
        _config = """
[global]
require-password = false
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name, config_defaults=_config_defaults)
        self.assertEqual(base_backend.check_access()[0], True)
        tf.close()

    ### TEST PROFILE DEFAULTS: get_profile_defaults()

    def test_getdefaultprofile(self):
        base_backend = self._init_base_backend()
        _expected_profile = {
            u'command': u'TERMINAL',
            u'defsndport': True,
            u'useiconv': False,
            u'iconvfrom': u'UTF-8',
            u'height': 600,
            u'export': u'',
            u'quality': 9,
            u'fullscreen': False,
            u'layout': u'',
            u'useexports': True,
            u'width': 800,
            u'speed': 2,
            u'soundsystem': u'pulse',
            u'print': True,
            u'type': u'auto',
            u'sndport': 4713,
            u'xinerama': True,
            u'variant': u'',
            u'usekbd': True,
            u'fstunnel': True,
            u'applications': [u'TERMINAL',u'WWWBROWSER',u'MAILCLIENT',u'OFFICE'],
            u'multidisp': False,
            u'sshproxyport': 22,
            u'sound': True,
            u'rootless': True,
            u'iconvto': u'UTF-8',
            u'soundtunnel': True,
            u'dpi': 96,
            u'sshport': 22,
            u'setdpi': 0,
            u'user': u'BROKER_USER',
            u'pack': u'16m-jpeg',
            u'host': [u'localhost'],
            u'directrdp': False,
        }
        _profile = base_backend.get_profile_defaults()
        self.assertEqual(len(_expected_profile.keys()), len(_profile.keys()))
        for key in _expected_profile.keys():
            self.assertTrue( ( key in _profile.keys() ) )
        for key in _profile.keys():
            self.assertTrue( ( key in _expected_profile.keys() and _profile[key] == _expected_profile[key] ) )

    ### TEST ACL DEFAULTS: get_acl_defaults()

    def test_getdefaultacls(self):
        base_backend = self._init_base_backend()
        _expected_acls = {
            'acl-users-allow': [],
            'acl-users-deny': [],
            'acl-users-order': '',
            'acl-groups-allow': [],
            'acl-groups-deny': [],
            'acl-groups-order': '',
            'acl-clients-allow': [],
            'acl-clients-deny': [],
            'acl-clients-order': '',
            'acl-any-order': 'deny-allow',
        }
        _acls = base_backend.get_acl_defaults()
        self.assertEqual(len(_expected_acls.keys()), len(_acls.keys()))
        for key in _expected_acls.keys():
            self.assertTrue( ( key in _acls.keys() ) )
        for key in _acls.keys():
            self.assertTrue( ( key in _expected_acls.keys() and _acls[key] == _expected_acls[key] ) )

    ### TEST ACL CHECK: check_profile_acls()

    def test_checkprofileacls_user_simpletests(self):
        base_backend = self._init_base_backend()
        username = 'foo'
        # no ACLs will grant access
        acls = {
            'acl-users-allow': [],
            'acl-users-deny': [],
            'acl-users-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-users-allow': [],
            'acl-users-deny': [],
            'acl-users-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-users-allow': ['ALL'],
            'acl-users-deny': [],
            'acl-users-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-users-allow': ['ALL'],
            'acl-users-deny': [],
            'acl-users-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-users-allow': ['foo'],
            'acl-users-deny': [],
            'acl-users-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-users-allow': ['foo'],
            'acl-users-deny': [],
            'acl-users-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-users-allow': [],
            'acl-users-deny': ['ALL'],
            'acl-users-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)
        acls = {
            'acl-users-allow': [],
            'acl-users-deny': ['ALL'],
            'acl-users-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)
        acls = {
            'acl-users-allow': [],
            'acl-users-deny': ['foo'],
            'acl-users-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)
        acls = {
            'acl-users-allow': [],
            'acl-users-deny': ['foo'],
            'acl-users-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)

    def test_checkprofileacls_user_combitests(self):
        base_backend = self._init_base_backend()
        username = 'foo'
        acls = {
            'acl-users-allow': ['foo'],
            'acl-users-deny': ['ALL'],
            'acl-users-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-users-allow': ['foo'],
            'acl-users-deny': ['ALL'],
            'acl-users-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)
        acls = {
            'acl-users-allow': ['ALL'],
            'acl-users-deny': ['foo'],
            'acl-users-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-users-allow': ['ALL'],
            'acl-users-deny': ['foo'],
            'acl-users-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)

    def test_testsuite_nameservice(self):

        _config_defaults = copy.deepcopy(x2gobroker.defaults.X2GOBROKER_CONFIG_DEFAULTS)
        _config = """
[global]
default-user-db = testsuite
default-group-db = testsuite

[broker_base]
enable = true
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name, config_defaults=_config_defaults)
        users = base_backend.get_users()
        users.sort()
        self.assertEqual(users, ['flip', 'kassandra', 'maja', 'thekla', 'willi'])
        groups = base_backend.get_groups()
        groups.sort()
        _expected_groups = ['bees', 'female', 'grasshoppers', 'male', 'spiders'] + ['flip', 'kassandra', 'maja', 'thekla', 'willi']
        _expected_groups.sort()
        self.assertEqual(groups, _expected_groups)

    def test_checkprofileacls_group_simpletests(self):
        _config_defaults = copy.deepcopy(x2gobroker.defaults.X2GOBROKER_CONFIG_DEFAULTS)
        _config = """
[global]
default-user-db = testsuite
default-group-db = testsuite

[broker_base]
enable = true
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name, config_defaults=_config_defaults)
        username = 'willi'
        acls = {
            'acl-groups-allow': ['ALL'],
            'acl-groups-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-groups-allow': ['male'],
            'acl-groups-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-groups-allow': ['ALL'],
            'acl-groups-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-groups-allow': [],
            'acl-groups-deny': ['ALL'],
            'acl-groups-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)
        acls = {
            'acl-groups-allow': [],
            'acl-groups-deny': ['ALL'],
            'acl-groups-order': 'deny-allow',
        }
        # now we set acl-users-allow to [] and we block all groups
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)
        acls = {
            'acl-groups-allow': [],
            'acl-groups-deny': ['ALL'],
            'acl-groups-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)

    def test_checkprofileacls_group_primarygroups(self):
        username_f = 'flip' # is a male grasshopper
        username_m = 'maja' # is a female bee
        username_w = 'willi' # is a drone (male bee)
        _config_defaults = copy.deepcopy(x2gobroker.defaults.X2GOBROKER_CONFIG_DEFAULTS)
        _config = """
[global]
default-user-db = testsuite
default-group-db = testsuite

[broker_base]
enable = true
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name, config_defaults=_config_defaults)
        acls = {
            'acl-groups-allow': ['bees','flip'],
            'acl-groups-deny': ['ALL'],
            'acl-groups-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), True)
        _config_defaults = copy.deepcopy(x2gobroker.defaults.X2GOBROKER_CONFIG_DEFAULTS)
        _config = """
[global]
default-user-db = testsuite
default-group-db = testsuite
ignore-primary-group-memberships = true

[broker_base]
enable = true
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name, config_defaults=_config_defaults)
        acls = {
            'acl-groups-allow': ['bees','flip'],
            'acl-groups-deny': ['ALL'],
            'acl-groups-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), True)
        _config_defaults = copy.deepcopy(x2gobroker.defaults.X2GOBROKER_CONFIG_DEFAULTS)
        _config = """
[global]
default-user-db = testsuite
default-group-db = testsuite
ignore-primary-group-memberships = false

[broker_base]
enable = true
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name, config_defaults=_config_defaults)
        acls = {
            'acl-groups-allow': ['bees','flip'],
            'acl-groups-deny': ['ALL'],
            'acl-groups-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), True)

    def test_checkprofileacls_group_combitests(self):
        _config_defaults = copy.deepcopy(x2gobroker.defaults.X2GOBROKER_CONFIG_DEFAULTS)
        _config = """
[global]
default-user-db = testsuite
default-group-db = testsuite

[broker_base]
enable = true
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name, config_defaults=_config_defaults)
        username_f = 'flip' # is a male grasshopper
        username_m = 'maja' # is a female bee
        username_w = 'willi' # is a drone (male bee)
        acls = {
            'acl-groups-allow': ['bees'],
            'acl-groups-deny': ['ALL'],
            'acl-groups-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), True)
        acls = {
            'acl-groups-allow': ['ALL'],
            'acl-groups-deny': ['bees'],
            'acl-groups-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), False)
        acls = {
            'acl-groups-allow': ['ALL'],
            'acl-groups-deny': ['bees'],
            'acl-groups-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), True)
        acls = {
            'acl-groups-allow': ['bees'],
            'acl-groups-deny': ['ALL'],
            'acl-groups-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), False)
        acls = {
            'acl-groups-allow': ['male'],
            'acl-groups-deny': ['bees'],
            'acl-groups-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), True)
        acls = {
            'acl-groups-allow': ['male'],
            'acl-groups-deny': ['bees'],
            'acl-groups-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), False)

    def test_checkprofileacls_userandgroup_combitests(self):
        _config_defaults = copy.deepcopy(x2gobroker.defaults.X2GOBROKER_CONFIG_DEFAULTS)
        _config = """
[global]
default-user-db = testsuite
default-group-db = testsuite

[broker_base]
enable = true
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name, config_defaults=_config_defaults)
        username_f = 'flip'
        username_k = 'kassandra'
        username_m = 'maja'
        username_t = 'thekla'
        username_w = 'willi'
        acls = {
            'acl-users-allow': ['flip'],
            'acl-users-deny': [],
            'acl-users-order': 'deny-allow',
            'acl-groups-allow': ['female','male'],
            'acl-groups-deny': ['spiders'],
            'acl-groups-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_k, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_t, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), True)
        acls = {
            'acl-users-allow': ['flip'],
            'acl-users-deny': [],
            'acl-users-order': 'deny-allow',
            'acl-groups-allow': ['female','male'],
            'acl-groups-deny': ['spiders'],
            'acl-groups-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_k, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_t, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), True)
        acls = {
            'acl-users-allow': ['flip'],
            'acl-users-deny': [],
            'acl-users-order': 'allow-deny',
            'acl-groups-allow': ['male','female'],
            'acl-groups-deny': ['spiders','grasshoppers'],
            'acl-groups-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_k, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_t, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), True)
        acls = {
            'acl-users-allow': [],
            'acl-users-deny': [],
            'acl-users-order': 'allow-deny',
            'acl-groups-allow': ['male','female'],
            'acl-groups-deny': ['spiders','grasshoppers'],
            'acl-groups-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_k, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_t, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), True)
        acls = {
            'acl-users-allow': ['flip', 'thekla'],
            'acl-users-deny': ['maja'],
            'acl-users-order': 'allow-deny',
            'acl-groups-allow': ['male','female'],
            'acl-groups-deny': ['spiders','grasshoppers'],
            'acl-groups-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_k, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_t, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), True)
        acls = {
            'acl-users-allow': ['flip', 'thekla'],
            'acl-users-deny': ['maja'],
            'acl-users-order': 'deny-allow',
            'acl-groups-allow': ['female'],
            'acl-groups-deny': ['spiders','grasshoppers'],
            'acl-groups-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_k, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_t, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), False)

    def test_clientaddress_recognition(self):
        base_backend = self._init_base_backend()
        ipv4_1 = '127.0.0.1'
        ipv4_2 = '10.0.0.1'
        ipv4_3 = '123.456.789.101'
        ipv6_1 = '::1'
        ipv6_2 = 'fe80::4f8:900:e5d:2'
        ipv6_3 = 'fe80:0000:0000:0000:04f8:0900:0e5d:0002'
        ipv6_4 = 'fe80:wxyz:0000:0000:04f8:0900:0e5d:0002'
        base_backend.set_client_address(ipv4_1)
        self.assertEqual(base_backend.get_client_address(), ipv4_1)
        self.assertEqual(base_backend.get_client_address_type(), 4)
        base_backend.set_client_address(ipv4_2)
        self.assertEqual(base_backend.get_client_address(), ipv4_2)
        self.assertEqual(base_backend.get_client_address_type(), 4)
        self.assertRaises(ValueError, base_backend.set_client_address, ipv4_3)
        base_backend.set_client_address(ipv6_1)
        self.assertEqual(base_backend.get_client_address(), ipv6_1)
        self.assertEqual(base_backend.get_client_address_type(), 6)
        base_backend.set_client_address(ipv6_2)
        self.assertEqual(base_backend.get_client_address(), ipv6_2)
        self.assertEqual(base_backend.get_client_address_type(), 6)
        base_backend.set_client_address(ipv6_3)
        self.assertEqual(base_backend.get_client_address(), 'fe80::4f8:900:e5d:2')
        self.assertEqual(base_backend.get_client_address_type(), 6)
        self.assertRaises(ValueError, base_backend.set_client_address, (ipv6_4))

    def test_checkprofileacls_clientipv4_simpletests(self):
        base_backend = self._init_base_backend()
        username = 'foo'
        base_backend.set_client_address('10.0.2.14')
        # no ACLs will grant access
        acls = {
            'acl-clients-allow': [],
            'acl-clients-deny': [],
            'acl-clients-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-clients-allow': [],
            'acl-clients-deny': [],
            'acl-clients-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-clients-allow': ['ALL'],
            'acl-clients-deny': [],
            'acl-clients-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-clients-allow': ['ALL'],
            'acl-clients-deny': [],
            'acl-clients-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-clients-allow': ['10.0.2.14'],
            'acl-clients-deny': [],
            'acl-clients-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-clients-allow': ['10.0.2.14'],
            'acl-clients-deny': [],
            'acl-clients-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-clients-allow': [],
            'acl-clients-deny': ['ALL'],
            'acl-clients-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)
        acls = {
            'acl-clients-allow': [],
            'acl-clients-deny': ['ALL'],
            'acl-clients-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)
        acls = {
            'acl-clients-allow': [],
            'acl-clients-deny': ['10.0.2.14'],
            'acl-clients-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)
        acls = {
            'acl-clients-allow': [],
            'acl-clients-deny': ['10.0.2.14'],
            'acl-clients-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)

    def test_checkprofileacls_clientipv4_combitests(self):
        base_backend = self._init_base_backend()
        username = 'foo'
        ipv4_1 = '10.0.2.14'
        ipv4_2 = '10.0.3.14'
        ipv4_3 = '8.8.8.8'

        base_backend.set_client_address(ipv4_1)
        # no ACLs will grant access
        acls = {
            'acl-clients-allow': ['10.0.2.0/24'],
            'acl-clients-deny': ['ALL'],
            'acl-clients-order': 'deny-allow',
        }
        base_backend.set_client_address(ipv4_1)
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        base_backend.set_client_address(ipv4_2)
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)
        base_backend.set_client_address(ipv4_3)
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)
        acls = {
            'acl-clients-allow': ['ALL'],
            'acl-clients-deny': ['10.0.2.0/24'],
            'acl-clients-order': 'allow-deny',
        }
        base_backend.set_client_address(ipv4_1)
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)
        base_backend.set_client_address(ipv4_2)
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        base_backend.set_client_address(ipv4_3)
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-clients-allow': ['10.0.2.0/24'],
            'acl-clients-deny': ['10.0.0.0/16', '10.0.3.0/24'],
            'acl-clients-order': 'deny-allow',
        }
        base_backend.set_client_address(ipv4_1)
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        base_backend.set_client_address(ipv4_2)
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)
        base_backend.set_client_address(ipv4_3)
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-clients-allow': ['10.0.0.0/16', '10.0.3.0/24'],
            'acl-clients-deny': ['10.0.2.0/24'],
            'acl-clients-order': 'allow-deny',
        }
        base_backend.set_client_address(ipv4_1)
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)
        base_backend.set_client_address(ipv4_2)
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        base_backend.set_client_address(ipv4_3)
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)

    def test_checkprofileacls_clientipv6_simpletests(self):
        base_backend = self._init_base_backend()
        username = 'foo'
        base_backend.set_client_address('fe80::4f8:900:e5d:2')
        # no ACLs will grant access
        acls = {
            'acl-clients-allow': [],
            'acl-clients-deny': [],
            'acl-clients-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-clients-allow': [],
            'acl-clients-deny': [],
            'acl-clients-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-clients-allow': ['ALL'],
            'acl-clients-deny': [],
            'acl-clients-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-clients-allow': ['ALL'],
            'acl-clients-deny': [],
            'acl-clients-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-clients-allow': ['fe80::4f8:900:e5d:2'],
            'acl-clients-deny': [],
            'acl-clients-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-clients-allow': ['fe80::4f8:900:e5d:2'],
            'acl-clients-deny': [],
            'acl-clients-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-clients-allow': [],
            'acl-clients-deny': ['ALL'],
            'acl-clients-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)
        acls = {
            'acl-clients-allow': [],
            'acl-clients-deny': ['ALL'],
            'acl-clients-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)
        acls = {
            'acl-clients-allow': [],
            'acl-clients-deny': ['fe80::4f8:900:e5d:2'],
            'acl-clients-order': 'deny-allow',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)
        acls = {
            'acl-clients-allow': [],
            'acl-clients-deny': ['fe80::4f8:900:e5d:2'],
            'acl-clients-order': 'allow-deny',
        }
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)

    def test_checkprofileacls_clientipv6_combitests(self):
        base_backend = self._init_base_backend()
        username = 'foo'
        ipv6_1 = 'fe80::4f8:900:e5d:2'
        ipv6_2 = 'fe80::1:4f8:900:e5d:2'
        ipv6_3 = '2001:1af8:4050::2'

        base_backend.set_client_address(ipv6_1)
        # no ACLs will grant access
        acls = {
            'acl-clients-allow': ['fe80::/64'],
            'acl-clients-deny': ['ALL'],
            'acl-clients-order': 'deny-allow',
        }
        base_backend.set_client_address(ipv6_1)
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        base_backend.set_client_address(ipv6_2)
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)
        base_backend.set_client_address(ipv6_3)
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)
        acls = {
            'acl-clients-allow': ['ALL'],
            'acl-clients-deny': ['fe80::/64'],
            'acl-clients-order': 'allow-deny',
        }
        base_backend.set_client_address(ipv6_1)
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)
        base_backend.set_client_address(ipv6_2)
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        base_backend.set_client_address(ipv6_3)
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-clients-allow': ['fe80::/64'],
            'acl-clients-deny': ['fe80::/56','fe80:0:0:1::/64'],
            'acl-clients-order': 'deny-allow',
        }
        base_backend.set_client_address(ipv6_1)
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        base_backend.set_client_address(ipv6_2)
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)
        base_backend.set_client_address(ipv6_3)
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        acls = {
            'acl-clients-allow': ['fe80::/56','fe80:0:0:1::/64'],
            'acl-clients-deny': ['fe80::/64'],
            'acl-clients-order': 'allow-deny',
        }
        base_backend.set_client_address(ipv6_1)
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)
        base_backend.set_client_address(ipv6_2)
        self.assertEqual(base_backend.check_profile_acls(username, acls), True)
        base_backend.set_client_address(ipv6_3)
        self.assertEqual(base_backend.check_profile_acls(username, acls), False)

    def test_checkprofileacls_userandgroupandclient_combitests(self):
        _config_defaults = copy.deepcopy(x2gobroker.defaults.X2GOBROKER_CONFIG_DEFAULTS)
        _config = """
[global]
default-user-db = testsuite
default-group-db = testsuite

[broker_base]
enable = true
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name, config_defaults=_config_defaults)
        username_f = 'flip'
        username_k = 'kassandra'
        username_m = 'maja'
        username_t = 'thekla'
        username_w = 'willi'
        ipv4_1 = '10.0.2.14'
        ipv4_2 = '10.0.3.14'
        ipv4_3 = '8.8.8.8'
        ipv6_1 = 'fe80::4f8:900:e5d:2'
        ipv6_2 = 'fe80::1:4f8:900:e5d:2'
        ipv6_3 = '2001:1af8:4050::2'
        acls = {
            'acl-users-allow': ['flip'],
            'acl-users-deny': [],
            'acl-users-order': 'deny-allow',
            'acl-groups-allow': ['female','male'],
            'acl-groups-deny': ['spiders'],
            'acl-groups-order': 'deny-allow',
            'acl-clients-allow': ['fe80:0:0:1::/64','10.0.3.0/24'],
            'acl-clients-deny': ['ALL'],
            'acl-clients-order': 'deny-allow',
        }
        base_backend.set_client_address(ipv4_1)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_k, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_t, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), False)
        base_backend.set_client_address(ipv4_2)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_k, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_t, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), True)
        base_backend.set_client_address(ipv4_3)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_k, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_t, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), False)
        base_backend.set_client_address(ipv6_1)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_k, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_t, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), False)
        base_backend.set_client_address(ipv6_2)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_k, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_t, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), True)
        base_backend.set_client_address(ipv6_3)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_k, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_t, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), False)
        acls = {
            'acl-users-allow': ['flip'],
            'acl-users-deny': [],
            'acl-users-order': 'deny-allow',
            'acl-groups-allow': ['female','male'],
            'acl-groups-deny': ['spiders'],
            'acl-groups-order': 'allow-deny',
            'acl-clients-allow': ['fe80::/64','10.0.2.0/24'],
            'acl-clients-deny': ['ALL'],
            'acl-clients-order': 'deny-allow',
        }
        base_backend.set_client_address(ipv4_1)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_k, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_t, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), True)
        base_backend.set_client_address(ipv4_2)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_k, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_t, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), False)
        base_backend.set_client_address(ipv4_3)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_k, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_t, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), False)
        base_backend.set_client_address(ipv6_1)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_k, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_t, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), True)
        base_backend.set_client_address(ipv6_2)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_k, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_t, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), False)
        base_backend.set_client_address(ipv6_3)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_k, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_t, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), False)
        acls = {
            'acl-users-allow': [],
            'acl-users-deny': [],
            'acl-users-order': 'allow-deny',
            'acl-groups-allow': ['male','female'],
            'acl-groups-deny': ['spiders','grasshoppers'],
            'acl-groups-order': 'allow-deny',
            'acl-clients-allow': ['ALL'],
            'acl-clients-deny': ['fe80::/56','10.0.0.0/8'],
            'acl-clients-order': 'allow-deny',
        }
        base_backend.set_client_address(ipv4_1)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_k, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_t, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), False)
        base_backend.set_client_address(ipv4_2)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_k, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_t, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), False)
        base_backend.set_client_address(ipv4_3)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_k, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_t, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), True)
        base_backend.set_client_address(ipv6_1)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_k, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_t, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), False)
        base_backend.set_client_address(ipv6_2)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_k, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_t, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), False)
        base_backend.set_client_address(ipv6_3)
        self.assertEqual(base_backend.check_profile_acls(username_f, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_k, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_m, acls), True)
        self.assertEqual(base_backend.check_profile_acls(username_t, acls), False)
        self.assertEqual(base_backend.check_profile_acls(username_w, acls), True)

    ### TEST CONFIGURATION: retrieve the initial authentication ID (my-cookie) from config file
    ###                     or separate (more secure) file (my-cookie-file).

    def test_get_mycookie(self):
        _config_defaults = copy.deepcopy(x2gobroker.defaults.X2GOBROKER_CONFIG_DEFAULTS)
        _config_defaults.update({'broker_base': {'enable': True, }, })

        _config = """
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name, config_defaults=_config_defaults)
        self.assertNotEqual(base_backend.get_my_cookie(), 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx')
        tf.close()
        _config = """
[global]
my-cookie = f57866be-bc67-4642-86b4-f644b54031c8
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name, config_defaults=_config_defaults)
        self.assertEqual(base_backend.get_my_cookie(), 'f57866be-bc67-4642-86b4-f644b54031c8')
        tf.close()
        _config = """
[global]
my-cookie = f57866be-bc67-4642-86b4-f644b54031c8
my-cookie-file = /etc/x2go/somefile/somewhere
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name, config_defaults=_config_defaults)
        self.assertEqual(base_backend.get_my_cookie(), 'f57866be-bc67-4642-86b4-f644b54031c8')
        tf.close()
        temp_fh = tempfile.NamedTemporaryFile()
        print >> temp_fh, 'd1b8043e-b748-48c4-907f-7c798c4dc746'
        temp_fh.seek(0)
        _config = """
[global]
my-cookie = f57866be-bc67-4642-86b4-f644b54031c8
my-cookie-file = %s
""" % temp_fh.name
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name, config_defaults=_config_defaults)
        self.assertNotEqual(base_backend.get_my_cookie(), 'f57866be-bc67-4642-86b4-f644b54031c8')
        self.assertEqual(base_backend.get_my_cookie(), 'd1b8043e-b748-48c4-907f-7c798c4dc746')
        temp_fh.close()
        tf.close()
        temp_fh = tempfile.NamedTemporaryFile()
        print >> temp_fh, """
# Some comment...
# Another comment...
d1b8043e-b748-48c4-907f-7c798c4dc746
d1b8043e-b748-48c4-907f-7c798c4dc747
# above hash is wrong...
"""
        temp_fh.seek(0)
        _config = """
[global]
my-cookie = f57866be-bc67-4642-86b4-f644b54031c8
my-cookie-file = %s
""" % temp_fh.name
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name, config_defaults=_config_defaults)
        self.assertNotEqual(base_backend.get_my_cookie(), 'f57866be-bc67-4642-86b4-f644b54031c8')
        self.assertEqual(base_backend.get_my_cookie(), 'd1b8043e-b748-48c4-907f-7c798c4dc746')
        temp_fh.close()
        tf.close()
        temp_fh = open('../../etc/broker/x2gobroker.authid')
        temp_fh.seek(0)
        _config = """
[global]
my-cookie = f57866be-bc67-4642-86b4-f644b54031c8
my-cookie-file = %s
""" % temp_fh.name
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        base_backend = base.X2GoBroker(config_file=tf.name, config_defaults=_config_defaults)
        self.assertNotEqual(base_backend.get_my_cookie(), 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx')
        self.assertEqual(base_backend.get_my_cookie(), 'f57866be-bc67-4642-86b4-f644b54031c8')
        temp_fh.close()
        tf.close()


def test_suite():
    from unittest import TestSuite, makeSuite
    suite = TestSuite()
    suite.addTest(makeSuite(TestX2GoBrokerBackendBase))
    return suite
