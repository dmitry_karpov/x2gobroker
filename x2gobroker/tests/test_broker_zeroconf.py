# -*- coding: utf-8 -*-

# Copyright (C) 2012-2015 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# X2Go Session Broker is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# X2Go Session Broker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import unittest
import copy
import tempfile

# Python X2GoBroker modules
import x2gobroker.brokers.zeroconf_broker

class TestX2GoBrokerBackendZeroconf(unittest.TestCase):

    ### TEST: list_profiles() method

    def test_profilelist(self):
        _save_maxDiff = self.maxDiff
        self.maxDiff = None
        list_of_profiles = {
            'unittest': {
                u'user': u'',
                u'defsndport': True,
                u'useiconv': False,
                u'iconvfrom': u'UTF-8',
                u'height': 600,
                u'export': u'',
                u'quality': 9,
                u'fullscreen': False,
                u'layout': u'',
                u'useexports': 1,
                u'width': 800,
                u'speed': 2,
                u'soundsystem': u'pulse',
                u'print': True,
                u'type': u'auto',
                u'sndport': 4713,
                u'xinerama': True,
                u'variant': u'',
                u'usekbd': True,
                u'fstunnel': True,
                u'applications': [u'TERMINAL',u'WWWBROWSER',u'MAILCLIENT',u'OFFICE',],
                u'host': u'localhost',
                u'multidisp': 0,
                u'sshproxyport': 22,
                u'sound': True,
                u'rootless': 0,
                u'name': u'LOCALHOST',
                u'iconvto': u'UTF-8',
                u'soundtunnel': True,
                u'command': 'KDE',
                u'dpi': 96,
                u'sshport': 22,
                u'setdpi': 0,
                u'pack': u'16m-jpeg',
                # make sure, hard-coded defaults end up in the list_profiles() output of the zeroconf backend, as well
                u'directrdp': False,
            },
        }
        zeroconf_backend = x2gobroker.brokers.zeroconf_broker.X2GoBroker()
        _profiles = zeroconf_backend.list_profiles('user_foo')
        self.assertEqual(len(_profiles.keys()), 1)
        _key = _profiles.keys()[0]
        # replace profile ID ,,unittest'' with the returned random profile ID (uuid hash)
        _test_profiles = {
            _key: list_of_profiles['unittest']
        }
        self.assertEqual(_profiles, _test_profiles)
        self.maxDiff = _save_maxDiff

    ### TEST: select_profile() method

    def test_desktopshell_uppercase(self):
        _config_defaults = copy.deepcopy(x2gobroker.defaults.X2GOBROKER_CONFIG_DEFAULTS)
        _config = """
[broker_zeroconf]
enable = true
desktop-shell = kDe
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        zeroconf_backend = x2gobroker.brokers.zeroconf_broker.X2GoBroker(config_file=tf.name, config_defaults=_config_defaults)
        _profiles = zeroconf_backend.list_profiles('user_foo')
        self.assertEqual(len(_profiles.keys()), 1)
        _key = _profiles.keys()[0]
        self.assertEqual(_profiles[_key]['command'], 'KDE')

    def test_sessionselection(self):
        _output = {
            'server': 'localhost',
            'port': 22,
        }
        zeroconf_backend = x2gobroker.brokers.zeroconf_broker.X2GoBroker()
        self.assertEqual(zeroconf_backend.select_session('any-profile-id'), _output)


def test_suite():
    from unittest import TestSuite, makeSuite
    suite = TestSuite()
    suite.addTest(makeSuite(TestX2GoBrokerBackendZeroconf))
    return suite
