# -*- coding: utf-8 -*-

# Copyright (C) 2012-2015 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# X2Go Session Broker is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# X2Go Session Broker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import unittest
import tempfile

# Python X2GoBroker modules
import x2gobroker.defaults
import x2gobroker.client.plain

from nose.tools import assert_equal, assert_true, assert_false

class args():
    def __init__(self):
        self.user = None
        self.auth_cookie = None
        self.task = None
        self.profile_id = None
        self.backend = 'base'


class TestX2GoBrokerClientPlainBase(unittest.TestCase):

    ### TEST RESPONSE: is enabled?

    def test_isenabled(self):

        _cf_bak = x2gobroker.defaults.X2GOBROKER_CONFIG

        a = args()
        _config = """
[broker_base]
enable = false
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        x2gobroker.defaults.X2GOBROKER_CONFIG = tf.name
        r = x2gobroker.client.plain.X2GoBrokerClient().get(a)
        assert_equal(r, None)
        tf.close()
        _config = """
[broker_base]
enable = true
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        _cf_bak = x2gobroker.defaults.X2GOBROKER_CONFIG
        x2gobroker.defaults.X2GOBROKER_CONFIG = tf.name
        r = x2gobroker.client.plain.X2GoBrokerClient().get(a)
        lines = r.split('\n')
        assert_equal(lines[1], "Access granted")
        tf.close()

        x2gobroker.defaults.X2GOBROKER_CONFIG = _cf_bak

    ### TEST RESPONSE: simple authentication (check_access)

    ### TEST TASK: listsessions (nothing should be returned for the base backend)

    def test_listsessions(self):

        _cf_bak = x2gobroker.defaults.X2GOBROKER_CONFIG

        a = args()
        _config = """
[broker_base]
enable = true
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        x2gobroker.defaults.X2GOBROKER_CONFIG = tf.name

        r = x2gobroker.client.plain.X2GoBrokerClient().get(a)

        assert_true('Access granted' in r)
        assert_false('START_USER_SESSIONS' in r)
        assert_false('END_USER_SESSIONS' in r)
        assert_false('<BR>' in r)
        assert_false('<br>' in r)
        assert_false('<BR />' in r)
        assert_false('<br />' in r)

        x2gobroker.defaults.X2GOBROKER_CONFIG = _cf_bak



def test_suite():
    from unittest import TestSuite, makeSuite
    suite = TestSuite()
    suite.addTest(makeSuite(TestX2GoBrokerClientPlainBase))
    return suite
