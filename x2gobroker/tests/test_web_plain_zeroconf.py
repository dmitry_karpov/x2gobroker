# -*- coding: utf-8 -*-

# Copyright (C) 2012-2015 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# X2Go Session Broker is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# X2Go Session Broker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import unittest
import tempfile
from paste.fixture import TestApp
from nose.tools import assert_equal
import tornado.wsgi

# Python X2GoBroker modules
import x2gobroker.defaults
import x2gobroker.web.plain

urls = ( ('/plain/(.*)', x2gobroker.web.plain.X2GoBrokerWeb,) ,)
application = tornado.wsgi.WSGIApplication(urls)

class TestX2GoBrokerWebPlainZeroconf(unittest.TestCase):

    ### TEST TASK: listsessions (you can influence the session command via the X2Go Broker's configurationfile)

    def test_listsessions_checkcommand(self):
        _config = """
[broker_zeroconf]
enable = true
auth-mech = testsuite
desktop-shell = KDE
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        x2gobroker.defaults.X2GOBROKER_CONFIG = tf.name
        testApp = TestApp(application)
        r = testApp.get('/plain/zeroconf/', params={'user': 'test', 'password': 'sweet',  'task': 'listsessions', }, expect_errors=True)
        assert_equal(r.status, 200)
        r.mustcontain('Access granted')
        r.mustcontain('START_USER_SESSIONS')
        r.mustcontain('command=KDE')
        r.mustcontain('END_USER_SESSIONS')
        r.mustcontain(no='<BR>',)
        r.mustcontain(no='<br>',)
        r.mustcontain(no='<BR />', )
        r.mustcontain(no='<br />', )
        tf.close()
        _config = """
[broker_zeroconf]
enable = true
auth-mech = testsuite
desktop-shell = GNOME
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        x2gobroker.defaults.X2GOBROKER_CONFIG = tf.name
        testApp = TestApp(application)
        r = testApp.get('/plain/zeroconf/', params={'user': 'test', 'password': 'sweet',  'task': 'listsessions', }, expect_errors=True)
        assert_equal(r.status, 200)
        r.mustcontain('Access granted')
        r.mustcontain('START_USER_SESSIONS')
        r.mustcontain('command=GNOME')
        r.mustcontain('END_USER_SESSIONS')
        r.mustcontain(no='<BR>',)
        r.mustcontain(no='<br>',)
        r.mustcontain(no='<BR />', )
        r.mustcontain(no='<br />', )
        tf.close()

    ### TEST TASK: selectsession (returns localhost as the only server, no SSH key, no session info)

    def test_selectsession(self):
        _config = """
[broker_zeroconf]
enable = true
auth-mech = testsuite
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        x2gobroker.defaults.X2GOBROKER_CONFIG = tf.name
        testApp = TestApp(application)
        r = testApp.get('/plain/zeroconf/', params={'user': 'test', 'password': 'sweet',  'task': 'selectsession', 'sid': 'LOCALHOST',}, expect_errors=True)
        assert_equal(r.status, 200)
        r.mustcontain('Access granted')
        r.mustcontain('SERVER:localhost:22')
        r.mustcontain(no='<BR>',)
        r.mustcontain(no='<br>',)
        r.mustcontain(no='<BR />', )
        r.mustcontain(no='<br />', )
        tf.close()


def test_suite():
    from unittest import TestSuite, makeSuite
    suite = TestSuite()
    suite.addTest(makeSuite(TestX2GoBrokerWebPlainZeroconf))
    return suite
