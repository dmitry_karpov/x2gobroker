# -*- coding: utf-8 -*-

# Copyright (C) 2012-2015 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# X2Go Session Broker is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# X2Go Session Broker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import unittest
import tempfile
from paste.fixture import TestApp
from nose.tools import assert_equal
import tornado.wsgi
import json

# Python X2GoBroker modules
import x2gobroker.defaults
import x2gobroker.web.uccs

urls = (
         ('/uccs/[a-zA-Z]*(/*)$', x2gobroker.web.uccs.X2GoBrokerWeb,),
         ('/uccs/(.*)/api/([0-9])(/*)$', x2gobroker.web.uccs.X2GoBrokerWebAPI,),
)
application = tornado.wsgi.WSGIApplication(urls)

class TestX2GoBrokerWebUccsZeroConf(unittest.TestCase):

    ### TEST TASK: listsessions (you can influence the session command via the X2Go Broker's configurationfile)

    def test_listsessions(self):
        _expected_result = {
            u'URL': u'http://localhost:8080/uccs/zeroconf',
            u'AdditionalManagementServers': [],
            u'Name': u'X2Go Session Broker',
            u'DefaultServer': u'LOCALHOST',
            u'RemoteDesktopServers': [
                {
                    u'Username': u'',
                    u'Protocol': u'x2go',
                    u'Name': u'LOCALHOST',
                    u'URL': u'http://localhost:22/',
                    u'SessionType': u'KDE',
                    u'SessionTypeRequired': True,
                    u'Password': u'',
                },
            ],
            u'URL': u'http://localhost:8080/uccs/zeroconf/',
        }
        _config = """
[global]
enable-uccs-output=true
check-credentials=false

[zeroconf]
enable = true
auth-mech = testsuite
desktop-shell = KDE
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        x2gobroker.defaults.X2GOBROKER_CONFIG = tf.name
        testApp = TestApp(application)

        #username = 'test'
        #password = 'sweet'
        #base64String = base64.encodestring('{username}:{password}'.format(username=username, password=password))[:-1]
        #authHeader =  "Basic {authcred}".format(authcred=base64String)
        #headers = {'Authorization': authHeader}
        headers = {}

        r = testApp.get('/uccs/zeroconf/api/4', headers=headers, expect_errors=True)
        assert_equal(r.status, 200)
        body = r.normal_body
        result = json.loads(body)
        self.assertEqual(_expected_result, result)
        tf.close()

    def test_listsessions_uppercase_desktopcommands(self):
        _expected_result = {
            u'URL': u'http://localhost:8080/uccs/zeroconf',
            u'AdditionalManagementServers': [],
            u'Name': u'X2Go Session Broker',
            u'DefaultServer': u'LOCALHOST',
            u'RemoteDesktopServers': [
                {
                    u'Username': u'',
                    u'Protocol': u'x2go',
                    u'Name': u'LOCALHOST',
                    u'URL': u'http://localhost:22/',
                    u'SessionType': u'KDE',
                    u'SessionTypeRequired': True,
                    u'Password': u'',
                },
            ],
            u'URL': u'http://localhost:8080/uccs/zeroconf/',
        }
        _config = """
[global]
enable-uccs-output=true
check-credentials=false

[zeroconf]
enable = true
auth-mech = testsuite
desktop-shell = kdE
"""
        tf = tempfile.NamedTemporaryFile()
        print >> tf, _config
        tf.seek(0)
        x2gobroker.defaults.X2GOBROKER_CONFIG = tf.name
        testApp = TestApp(application)

        r = testApp.get('/uccs/zeroconf/api/4', headers=headers, expect_errors=True)
        assert_equal(r.status, 200)
        body = r.normal_body
        result = json.loads(body)
        self.assertEqual(_expected_result, result)
        tf.close()


def test_suite():
    from unittest import TestSuite#, makeSuite
    suite = TestSuite()
    #suite.addTest(makeSuite(TestX2GoBrokerWebUccsZeroConf))
    return suite
