# -*- coding: utf-8 -*-

# Copyright (C) 2012-2015 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# X2Go Session Broker is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# X2Go Session Broker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

# modules
from socket import error
import getpass
import pam

# X2Go Session Broker modules
import x2gobroker.authservice
from x2gobroker.loggers import logger_error

class X2GoBrokerAuthMech(object):

    def authenticate(self, username, password, **kwargs):

        if username and password:
            try:
                # query the X2Go Session Broker's PAM Auth Service
                if x2gobroker.authservice.authenticate(username, password, service="x2gobroker"):
                    return True

            except error:
                logger_error.error('Authentication against authentication service failed, trying direct PAM authentication (which is likely to fail on most PAM setups).')
                logger_error.error('Make sure the current user ({user}) is allowed to use the PAM authentication mechanism.'.format(user=getpass.getuser()))
                # fallback to direct PAM authentication against the PAM service ,,x2gobroker''
                if pam.authenticate(username, password, service="x2gobroker"):
                    return True

        return False
