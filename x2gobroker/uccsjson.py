# -*- coding: utf-8 -*-

# Copyright (C) 2012-2015 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# X2Go Session Broker is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# X2Go Session Broker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

try: import simplejson as json
except ImportError: import json

def convert_to_builtin_type(obj):
    """\
    Helper function for converting Python objects to dictionaries.
    Used for doing JSON dumps.

    """
    d = { }
    d.update(obj.__dict__)
    return d

class ManagementServer():
    """\
    Base class for generating UCCS compatible JSON object.

    """
    def __init__(self, url, name):
        """\
        Initializ instance.

        @param url: URL of the UCCS broker server
        @type url: C{unicode}
        @param name: human-readable, descriptive server name
        @type name: C{unicode}

        """
        self.RemoteDesktopServers = []
        self.AdditionalManagementServers = []
        self.URL = unicode(url)
        self.URL = '{url}/'.format(url=self.URL.rstrip('/'))
        self.Name = unicode(name)

    def set_default(self, ts_name):
        """\
        Define the default (terminal) server instance.

        @param ts_name: name of the terminal server that is to be set as default
        @type ts_name: C{unicode}

        """
        if isinstance(ts_name, str):
            self.DefaultServer = unicode(ts_name)
        elif isinstance(ts_name, unicode):
            self.DefaultServer = ts_name
        else:
            raise TypeError("set_default expects a string argument")

    def add_terminalserver(self, server):
        """\
        Add a terminal server to this management server object.

        @param server: instance of class L{RDPServer} or L{X2GoServer}.
        @type server: C{obj}

        """
        self.RemoteDesktopServers.append(server)

    # NOT USED!!!
    #def add_additional_management_server(self, amserver):
    #    if isinstance(amserver, AdditionalManagementServer):
    #        self.AdditionalManagementServers.append(amserver)
    #    else:
    #        raise TypeError("add_additional_management_server expects a "\
    #            "AdditionalManagementServer argument")

    def toJson(self):
        """\
        Dump this instance as JSON object.

        """
        return json.dumps(self, default=convert_to_builtin_type, sort_keys=True, indent=2)


# NOT USED!!!
#class AdditionalManagementServer():
#    def __init__(self, url, name):
#        self.URL = url
#        self.Name = name


class RDPServer():
    """\
    Instantiate a UCCS compatible RDP server session profile object.

    """
    def __init__(self, host, name, username='', password=''):
        """\
        @param host: hostname of RDP server host
        @type host: C{unicode}
        @param name: session profile name
        @type name: C{unicode}
        @param username: username to be used for login
        @type username: C{unicode}
        @param password: password to be used for login
        @type password: C{unicode}

        """
        self.URL = 'http://{url}/'.format(url=unicode(host))
        self.Name = unicode(name)
        self.Protocol = u'rdp'
        self.DomainRequired = True
        self.Username = unicode(username)
        self.Password = unicode(password)

    def set_domain(self, domain):
        """\
        Set the domain for this RDP server.

        @param domain: the domain name to be set
        @type domain: C{unicode}

        @raise TypeError: domain has to be C{str} or C{unicode}

        """
        if isinstance(domain, str):
            self.WindowsDomain = unicode(domain)
        elif isinstance(domain, unicode):
            self.WindowsDomain = domain
        else:
            raise TypeError("set_domain() expects a string or unicode argument")

    def toJson(self):
        """\
        Dump this instance as JSON object.

        """
        return json.dumps(self, default=convert_to_builtin_type, sort_keys=True, indent=2)


class ICAServer():
    """\
    Instantiate a UCCS compatible ICA server session profile object.

    """
    def __init__(self, host, name, username='', password=''):
        """\
        @param host: hostname of ICA server host
        @type host: C{unicode}
        @param name: session profile name
        @type name: C{unicode}
        @param username: username to be used for login
        @type username: C{unicode}
        @param password: password to be used for login
        @type password: C{unicode}

        """
        self.URL = 'http://{url}/'.format(url=unicode(host))
        self.Name = unicode(name)
        self.Protocol = u'ica'
        self.DomainRequired = unicode(True)
        self.Username = unicode(username)
        self.Password = unicode(password)

    def set_domain(self, domain):
        """\
        Set the domain for this ICA server.

        @param domain: the domain name to be set
        @type domain: C{unicode}

        @raise TypeError: domain has to be C{str} or C{unicode}

        """
        if isinstance(domain, str):
            self.WindowsDomain = unicode(domain)
        elif isinstance(domain, unicode):
            self.WindowsDomain = domain
        else:
            raise TypeError("set_domain() expects a string or unicode argument")

    def toJson(self):
        """\
        Dump this instance as JSON object.

        """
        return json.dumps(self, default=convert_to_builtin_type, sort_keys=True, indent=2)


class X2GoServer():
    """\
    Instantiate a UCCS compatible X2Go Server session profile object.

    """
    def __init__(self, host, name, username='', password=''):
        """\
        @param host: hostname of X2Go Server host
        @type host: C{unicode}
        @param name: session profile name
        @type name: C{unicode}
        @param username: username to be used for login
        @type username: C{unicode}
        @param password: password to be used for login
        @type password: C{unicode}

        """
        self.URL = 'http://{url}/'.format(url=unicode(host))
        self.Name = unicode(name)
        self.Protocol = u'x2go'
        self.SessionTypeRequired = True
        self.Username = unicode(username)
        self.Password = unicode(password)

    def set_session_type(self, session_type):
        """\
        Set the session type to be used with this X2Go Server.

        @param session_type: the session type to be set
        @type session_type: C{unicode}

        @raise TypeError: session_type has to be C{str} or C{unicode}

        """
        if isinstance(session_type, str):
            self.SessionType = unicode(session_type)
        elif isinstance(session_type, unicode):
            self.SessionType = session_type
        else:
            raise TypeError("set_session_type() expects a string or unicode argument")

    def toJson(self):
        """\
        Dump this instance as JSON object.

        """
        return json.dumps(self, default=convert_to_builtin_type, sort_keys=True, indent=2)

