# -*- coding: utf-8 -*-

# This file is part of the  X2Go Project - http://www.x2go.org
# Copyright (C) 2012-2015 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# X2Go Session Broker is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# X2Go Session Broker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import socket

# X2Go Session Broker modules
import x2gobroker.defaults
from x2gobroker.loggers import logger_broker


def authenticate(username, password, service="x2gobroker"):
    s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    logger_broker.debug('connecting to authentication service socket {socket}'.format(socket=x2gobroker.defaults.X2GOBROKER_AUTHSERVICE_SOCKET))
    s.connect(x2gobroker.defaults.X2GOBROKER_AUTHSERVICE_SOCKET)
    # FIXME: somehow logging output disappears after we have connected to the socket file...
    logger_broker.debug('sending username={username}, password=<hidden>, service={service} to authentication service'.format(username=username, service=service))
    s.send('{username}\r{password}\r{service}\n'.format(username=username, password=password, service=service))
    result = s.recv(1024)
    s.close()
    if result.startswith('ok'):
        logger_broker.info('authentication against PAM service »{service}« succeeded for user »{username}«'.format(username=username, service=service))
        return True
    logger_broker.info('authentication against service »{service}« failed for user »{username}«'.format(username=username, service=service))
    return False
