# This file is part of the  X2Go Project - http://www.x2go.org
# Copyright (C) 2012-2015 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# X2Go Session Broker is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# X2Go Session Broker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

# modules
import datetime
import types
import random
import tornado.web

# Python X2Go Broker modules
import x2gobroker.defaults

from x2gobroker.loggers import logger_broker, logger_error
import x2gobroker.uccsjson
import x2gobroker.basicauth


def credentials_validate(username, password):

    import x2gobroker.brokers.base_broker
    # FIXME: with the below hack, the backend broker detection in X2GoBrokerWeb is disabled, only global options
    #        from x2gobroker.conf are available here...
    broker = x2gobroker.brokers.base_broker.X2GoBroker()
    broker.enable()
    access, next_cookie = broker.check_access(username=username, password=password)
    # UCCS only allows email addresses for remote login
    if not access and "@" in username:
        username = username.split('@')[0]
        access, next_cookie = broker.check_access(username=username, password=password)
    if username == 'check-credentials' and password == 'FALSE':
        username = 'anonymous'
    return username, access


class _RequestHandler(tornado.web.RequestHandler):
    def _handle_request_exception(self, e):
        logger_error.error('HTTP request error: {error_msg}'.format(error_msg=unicode(e)))


class X2GoBrokerWeb(_RequestHandler):

    def get(self, path):
        if x2gobroker.defaults.X2GOBROKER_DEBUG:
            logger_broker.warn('GET http request detected, if unwanted: disable X2GOBROKER_DEBUG')
            return self.head(path)
        raise tornado.web.HTTPError(405)

    def head(self, path):
        self.write(unicode(datetime.datetime.utcnow()))
        return


@x2gobroker.basicauth.require_basic_auth('Authentication required', credentials_validate)
class X2GoBrokerWebAPI(tornado.web.RequestHandler):

    http_header_items = {
        'Content-Type': 'text/plain; charset=utf-8',
        'Expires': '+1h',
    }

    def _gen_http_header(self):

        for http_header_item in self.http_header_items.keys():
            self.set_header(http_header_item, self.http_header_items[http_header_item])

    def get(self, *args, **kwargs):

        self._gen_http_header()

        backend = args[0]
        api_version = args[1]
        try:
            api_version = int(api_version)
        except TypeError:
            api_version = 4

        if not backend:
            self.backend = x2gobroker.defaults.X2GOBROKER_DEFAULT_BACKEND
        else:
            self.backend = backend.rstrip('/')

        try:
            # dynamically detect broker backend from given URL
            exec("import x2gobroker.brokers.{backend}_broker".format(backend=self.backend))
            exec("self.broker_backend = x2gobroker.brokers.{backend}_broker.X2GoBroker()".format(backend=self.backend))
        except ImportError:
            # throw a 404 if the backend does not exist
            raise tornado.web.HTTPError(404)

        global_config = self.broker_backend.get_global_config()

        # throw a 404 if the WebUI is not enabled
        if not global_config['enable-uccs-output']:
            raise tornado.web.HTTPError(404)

        # if the broker backend is disabled in the configuration, pretend to have nothing on offer
        if not self.broker_backend.is_enabled():
            raise tornado.web.HTTPError(404)

        # set the client address for the broker backend
        ip = self.request.remote_ip
        if ip:
            self.broker_backend.set_client_address(ip)
            logger_broker.info('client address is {address}'.format(address=ip))
        elif not x2gobroker.defaults.X2GOBROKER_DEBUG:
            # if the client IP is not set, we pretend to have nothing on offer
            logger_error.error('client could not provide an IP address, pretending: 404 Not Found')
            raise tornado.web.HTTPError(404)

        ###
        ### CONFIRM SUCCESSFUL AUTHENTICATION FIRST
        ###

        try:
            username = kwargs['basicauth_user']
        except KeyError:
            raise tornado.web.HTTPError(401)

        logger_broker.debug ('Authenticated as username: {username}, with password: <hidden>'.format(username=username))

        ###
        ### GENERATE UCCS JSON TREE
        ###

        output = ''

        profile_ids = self.broker_backend.get_profile_ids_for_user(username)
        urlbase = self.broker_backend.get_global_value('my-uccs-url-base').rstrip('/')
        ms = x2gobroker.uccsjson.ManagementServer('{urlbase}/uccs/{backend}/'.format(urlbase=urlbase, backend=backend), 'X2Go Session Broker')

        profile_ids.sort()

        for profile_id in profile_ids:

            profile = self.broker_backend.get_profile_for_user(profile_id, username, broker_frontend='uccs')
            hosts = profile[u'host']
            if type(hosts) == types.UnicodeType:
                hosts = [hosts]

            if profile[u'directrdp']:
                ts = x2gobroker.uccsjson.RDPServer(
                        host='{hostname}'.format(hostname=hosts[0]),
                        name=profile[u'name'],
                        username=profile[u'user'],
                )
                ts.set_domain('LOCAL')
            else:
                _hostname = random.choice(hosts)
                _port = profile[u'sshport']
                if profile.has_key('sshport={hostname}'.format(hostname=_hostname)):
                    _port = profile['sshport={hostname}'.format(hostname=_hostname)]
                if profile.has_key('host={hostname}'.format(hostname=_hostname)):
                    _hostname = profile['host={hostname}'.format(hostname=_hostname)]
                ts = x2gobroker.uccsjson.X2GoServer(
                        host='{hostname}:{port}'.format(hostname=_hostname, port=_port),
                        name=profile[u'name'],
                        username=profile[u'user'],
                )
                _cmd = profile['command']
                if _cmd.upper() in x2gobroker.defaults.X2GO_DESKTOP_SESSIONS:
                    _cmd = _cmd.upper()
                ts.set_session_type(_cmd)
            ms.add_terminalserver(ts)
            ms.set_default(ts.Name)

        output += ms.toJson()

        self.write(output)
        return

