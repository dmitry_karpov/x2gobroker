# -*- coding: utf-8 -*-

# Copyright (C) 2012-2015 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# X2Go Session Broker is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# X2Go Session Broker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

# modules
import os
import uuid
import socket
import pwd, grp

import logging
from loggers import logger_broker, logger_access, logger_error, X2GOBROKER_DAEMON_USER
from loggers import iniconfig_loaded
if iniconfig_loaded:
    from loggers import iniconfig, iniconfig_section

X2GOBROKER_USER =  pwd.getpwuid(os.geteuid())[0]
X2GOBROKER_GROUP =  grp.getgrgid(pwd.getpwuid(os.geteuid())[3])[0]
os.environ['HOME'] = pwd.getpwuid(os.geteuid())[5]

if os.environ.has_key('X2GOBROKER_DAEMON_GROUP'):
    X2GOBROKER_DAEMON_GROUP=os.environ['X2GOBROKER_DAEMON_GROUP']
elif iniconfig_loaded and iniconfig.has_option(iniconfig_section, 'X2GOBROKER_DAEMON_GROUP'):
    X2GOBROKER_DAEMON_GROUP=iniconfig.get(iniconfig_section, 'X2GOBROKER_DAEMON_GROUP')
elif iniconfig_loaded and iniconfig.has_option('common', 'X2GOBROKER_DAEMON_GROUP'):
    X2GOBROKER_DAEMON_GROUP=iniconfig.get('common', 'X2GOBROKER_DAEMON_GROUP')
else:
    X2GOBROKER_DAEMON_GROUP="x2gobroker"
if os.environ.has_key('X2GOBROKER_AGENT_USER'):
    X2GOBROKER_AGENT_USER=os.environ['X2GOBROKER_AGENT_USER']
elif iniconfig_loaded and iniconfig.has_option(iniconfig_section, 'X2GOBROKER_AGENT_USER'):
    X2GOBROKER_AGENT_USER=iniconfig.get(iniconfig_section, 'X2GOBROKER_AGENT_USER')
elif iniconfig_loaded and iniconfig.has_option('common', 'X2GOBROKER_AGENT_USER'):
    X2GOBROKER_AGENT_USER=iniconfig.get('common', 'X2GOBROKER_AGENT_USER')
else:
    X2GOBROKER_AGENT_USER="x2gobroker"

###
### dynamic default values, influencable through os.environ...
###

if os.environ.has_key('X2GOBROKER_DEBUG'):
    X2GOBROKER_DEBUG = ( os.environ['X2GOBROKER_DEBUG'].lower() in ('1', 'on', 'true', 'yes', ) )
elif iniconfig_loaded and iniconfig.has_option(iniconfig_section, 'X2GOBROKER_DEBUG'):
    X2GOBROKER_DEBUG=iniconfig.get(iniconfig_section, 'X2GOBROKER_DEBUG')
elif iniconfig_loaded and iniconfig.has_option('common', 'X2GOBROKER_DEBUG'):
    X2GOBROKER_DEBUG=iniconfig.get('common', 'X2GOBROKER_DEBUG')
else:
    X2GOBROKER_DEBUG = False
if os.environ.has_key('X2GOBROKER_DEBUG_INTERACTIVELY'):
    X2GOBROKER_DEBUG_INTERACTIVELY = ( os.environ['X2GOBROKER_DEBUG_INTERACTIVELY'].lower() in ('1', 'on', 'true', 'yes', ) )
elif iniconfig_loaded and iniconfig.has_option(iniconfig_section, 'X2GOBROKER_DEBUG_INTERACTIVELY'):
    X2GOBROKER_DEBUG_INTERACTIVELY=iniconfig.get(iniconfig_section, 'X2GOBROKER_DEBUG_INTERACTIVELY')
elif iniconfig_loaded and iniconfig.has_option('common', 'X2GOBROKER_DEBUG_INTERACTIVELY'):
    X2GOBROKER_DEBUG_INTERACTIVELY=iniconfig.get('common', 'X2GOBROKER_DEBUG_INTERACTIVELY')
else:
    X2GOBROKER_DEBUG_INTERACTIVELY = False
if os.environ.has_key('X2GOBROKER_TESTSUITE'):
    X2GOBROKER_TESTSUITE = ( os.environ['X2GOBROKER_TESTSUITE'].lower() in ('1', 'on', 'true', 'yes', ) )
elif iniconfig_loaded and iniconfig.has_option(iniconfig_section, 'X2GOBROKER_TESTSUITE'):
    X2GOBROKER_TESTSUITE=iniconfig.get(iniconfig_section, 'X2GOBROKER_TESTSUITE')
elif iniconfig_loaded and iniconfig.has_option('common', 'X2GOBROKER_TESTSUITE'):
    X2GOBROKER_TESTSUITE=iniconfig.get('common', 'X2GOBROKER_TESTSUITE')
else:
    X2GOBROKER_TESTSUITE = False

# enforce debugging for interactive usage
if X2GOBROKER_USER != X2GOBROKER_DAEMON_USER:
    X2GOBROKER_DEBUG = True

# raise log levels to CRITICAL if we are running the unittests...
if X2GOBROKER_TESTSUITE:
    logger_broker.setLevel(logging.CRITICAL)
    logger_access.setLevel(logging.CRITICAL)
    logger_error.setLevel(logging.CRITICAL)

if os.environ.has_key('X2GOBROKER_CONFIG'):
    X2GOBROKER_CONFIG = os.environ['X2GOBROKER_CONFIG']
elif iniconfig_loaded and iniconfig.has_option(iniconfig_section, 'X2GOBROKER_CONFIG'):
    X2GOBROKER_CONFIG=iniconfig.get(iniconfig_section, 'X2GOBROKER_CONFIG')
elif iniconfig_loaded and iniconfig.has_option('common', 'X2GOBROKER_CONFIG'):
    X2GOBROKER_CONFIG=iniconfig.get('common', 'X2GOBROKER_CONFIG')
else:
    X2GOBROKER_CONFIG = "/etc/x2go/x2gobroker.conf"

if os.environ.has_key('X2GOBROKER_SESSIONPROFILES'):
    X2GOBROKER_SESSIONPROFILES = os.environ['X2GOBROKER_SESSIONPROFILES']
elif iniconfig_loaded and iniconfig.has_option(iniconfig_section, 'X2GOBROKER_SESSIONPROFILES'):
    X2GOBROKER_SESSIONPROFILES=iniconfig.get(iniconfig_section, 'X2GOBROKER_SESSIONPROFILES')
elif iniconfig_loaded and iniconfig.has_option('common', 'X2GOBROKER_SESSIONPROFILES'):
    X2GOBROKER_SESSIONPROFILES=iniconfig.get('common', 'X2GOBROKER_SESSIONPROFILES')
else:
    X2GOBROKER_SESSIONPROFILES = "/etc/x2go/broker/x2gobroker-sessionprofiles.conf"

if os.environ.has_key('X2GOBROKER_AGENT_CMD'):
    X2GOBROKER_AGENT_CMD = os.environ['X2GOBROKER_AGENT_CMD']
elif iniconfig_loaded and iniconfig.has_option(iniconfig_section, 'X2GOBROKER_AGENT_CMD'):
    X2GOBROKER_AGENT_CMD=iniconfig.get(iniconfig_section, 'X2GOBROKER_AGENT_CMD')
elif iniconfig_loaded and iniconfig.has_option('common', 'X2GOBROKER_AGENT_CMD'):
    X2GOBROKER_AGENT_CMD=iniconfig.get('common', 'X2GOBROKER_AGENT_CMD')
else:
    X2GOBROKER_AGENT_CMD = "/usr/lib/x2go/x2gobroker-agent"

if os.environ.has_key('X2GOBROKER_AUTHSERVICE_SOCKET'):
    X2GOBROKER_AUTHSERVICE_SOCKET=os.environ['X2GOBROKER_AUTHSERVICE_SOCKET']
elif iniconfig_loaded and iniconfig.has_option(iniconfig_section, 'X2GOBROKER_AUTHSERVICE_SOCKET'):
    X2GOBROKER_AUTHSERVICE_SOCKET=iniconfig.get(iniconfig_section, 'X2GOBROKER_AUTHSERVICE_SOCKET')
elif iniconfig_loaded and iniconfig.has_option('common', 'X2GOBROKER_AUTHSERVICE_SOCKET'):
    X2GOBROKER_AUTHSERVICE_SOCKET=iniconfig.get('common', 'X2GOBROKER_AUTHSERVICE_SOCKET')
else:
    if os.path.isdir('/run/x2gobroker'):
        RUNDIR = '/run'
    else:
        RUNDIR = '/var/run/x2gobroker'
    X2GOBROKER_AUTHSERVICE_SOCKET="{run}/x2gobroker/x2gobroker-authservice.socket".format(run=RUNDIR)

if os.environ.has_key('X2GOBROKER_LOADCHECKER_SOCKET'):
    X2GOBROKER_LOADCHECKER_SOCKET=os.environ['X2GOBROKER_LOADCHECKER_SOCKET']
elif iniconfig_loaded and iniconfig.has_option(iniconfig_section, 'X2GOBROKER_LOADCHECKER_SOCKET'):
    X2GOBROKER_LOADCHECKER_SOCKET=iniconfig.get(iniconfig_section, 'X2GOBROKER_LOADCHECKER_SOCKET')
elif iniconfig_loaded and iniconfig.has_option('common', 'X2GOBROKER_LOADCHECKER_SOCKET'):
    X2GOBROKER_LOADCHECKER_SOCKET=iniconfig.get('common', 'X2GOBROKER_LOADCHECKER_SOCKET')
else:
    if os.path.isdir('/run/x2gobroker'):
        RUNDIR = '/run'
    else:
        RUNDIR = '/var/run/x2gobroker'
    X2GOBROKER_LOADCHECKER_SOCKET="{run}/x2gobroker/x2gobroker-loadchecker.socket".format(run=RUNDIR)

if os.environ.has_key('X2GOBROKER_DEFAULT_BACKEND'):
    X2GOBROKER_DEFAULT_BACKEND = os.environ['X2GOBROKER_DEFAULT_BACKEND']
elif iniconfig_loaded and iniconfig.has_option(iniconfig_section, 'X2GOBROKER_DEFAULT_BACKEND'):
    X2GOBROKER_DEFAULT_BACKEND=iniconfig.get(iniconfig_section, 'X2GOBROKER_DEFAULT_BACKEND')
elif iniconfig_loaded and iniconfig.has_option('common', 'X2GOBROKER_DEFAULT_BACKEND'):
    X2GOBROKER_DEFAULT_BACKEND=iniconfig.get('common', 'X2GOBROKER_DEFAULT_BACKEND')
else:
    X2GOBROKER_DEFAULT_BACKEND = "inifile"

if os.environ.has_key('DAEMON_BIND_ADDRESS'):
    DAEMON_BIND_ADDRESS = os.environ['DAEMON_BIND_ADDRESS']
elif iniconfig_loaded and iniconfig.has_option(iniconfig_section, 'DAEMON_BIND_ADDRESS'):
    DAEMON_BIND_ADDRESS = iniconfig.get(iniconfig_section, 'DAEMON_BIND_ADDRESS')
elif iniconfig_loaded and iniconfig.has_option('daemon', 'DAEMON_BIND_ADDRESS'):
    DAEMON_BIND_ADDRESS = iniconfig.get('daemon', 'DAEMON_BIND_ADDRESS')
else:
    DAEMON_BIND_ADDRESS = ""

if os.environ.has_key('X2GOBROKER_SSL_CERTFILE'):
    X2GOBROKER_SSL_CERTFILE = os.environ['X2GOBROKER_SSL_CERTFILE']
elif iniconfig_loaded and iniconfig.has_option(iniconfig_section, 'X2GOBROKER_SSL_CERTFILE'):
    X2GOBROKER_SSL_CERTFILE = iniconfig.get(iniconfig_section, 'X2GOBROKER_SSL_CERTFILE')
elif iniconfig_loaded and iniconfig.has_option('daemon', 'X2GOBROKER_SSL_CERTFILE'):
    X2GOBROKER_SSL_CERTFILE = iniconfig.get('daemon', 'X2GOBROKER_SSL_CERTFILE')
else:
    X2GOBROKER_SSL_CERTFILE = ""

if os.environ.has_key('X2GOBROKER_SSL_KEYFILE'):
    X2GOBROKER_SSL_KEYFILE = os.environ['X2GOBROKER_SSL_KEYFILE']
elif iniconfig_loaded and iniconfig.has_option(iniconfig_section, 'X2GOBROKER_SSL_KEYFILE'):
    X2GOBROKER_SSL_KEYFILE = iniconfig.get(iniconfig_section, 'X2GOBROKER_SSL_KEYFILE')
elif iniconfig_loaded and iniconfig.has_option('daemon', 'X2GOBROKER_SSL_KEYFILE'):
    X2GOBROKER_SSL_KEYFILE = iniconfig.get('daemon', 'X2GOBROKER_SSL_KEYFILE')
else:
    X2GOBROKER_SSL_KEYFILE = ""

###
### static / hard-coded defaults
###

if socket.gethostname().find('.') >= 0:
    X2GOBROKER_HOSTNAME = socket.gethostname()
else:
    X2GOBROKER_HOSTNAME = socket.gethostbyaddr(socket.gethostname())[0]

# the home directory of the user that the daemon/cgi runs as
X2GOBROKER_HOME = os.path.normpath(os.path.expanduser('~{broker_uid}'.format(broker_uid=X2GOBROKER_DAEMON_USER)))

# defaults for X2Go Sessino Broker configuration file
X2GOBROKER_CONFIG_DEFAULTS = {
    'global': {
        # legacy support for X2Go Session Broker << 0.0.3.0
        # the check-credentials parameter has been slit up into the two params above
        u'check-credentials': True,
        # use these two instead of check-credentials...
        u'require-password': True,
        u'require-cookie': False,
        u'use-static-cookie': False,
        u'auth-timeout': 36000,
        u'cookie-directory': '/var/lib/x2gobroker/cookies',
        u'verify-ip': True,
        u'pre_auth_scripts': [],
        u'post_auth_scripts': [],
        u'select_session_scripts': [],
        u'my-cookie': uuid.uuid4(),
        u'my-cookie-file': '/etc/x2go/broker/x2gobroker.authid',
        u'enable-plain-output': True,
        u'enable-json-output': True,
        u'enable-uccs-output': False,
        u'my-uccs-url-base': 'http://localhost:8080/',
        u'default-auth-mech': u'pam',
        u'default-user-db': u'libnss',
        u'default-group-db': u'libnss',
        u'ignore-primary-group-memberships': True,
        u'default-session-autologin': False,
        u'default-authorized-keys': u'%h/.x2go/authorized_keys',
        u'default-sshproxy-authorized-keys': u'%h/.x2go/authorized_keys',
        u'default-agent-query-mode': u'NONE',
        u'default-portscan-x2goservers': True,
        u'default-use-load-checker': False,
        u'load-checker-intervals': 300,
    },
    'broker_base': {
        u'enable': False,
    },
    'broker_zeroconf': {
        u'enable': False,
        u'auth-mech': u'pam',
        u'user-db': u'libnss',
        u'group-db': u'libnss',
        u'desktop-shell': u'KDE',
        u'load-checker': False,
    },
    'broker_inifile': {
        u'enable': True,
        u'session-profiles': u'/etc/x2go/broker/x2gobroker-sessionprofiles.conf',
        u'auth-mech': u'',
        u'user-db': u'',
        u'group-db': u'',
        u'use-load-checker': True,
    },
    'broker_ldap': {
        u'enable': False,
        u'auth-mech': u'ldap',
        u'user-db': u'ldap',
        u'group-db': u'ldap',
        u'uri': u'ldap://localhost:389',
        u'base': u'dc=example,dc=org',
        u'user-search-filter': u'(&(objectClass=posixAccount)(uid=*))',
        u'host-search-filter': u'(&(objectClass=ipHost)(serial=X2GoServer)(cn=*))',
        u'group-search-filter': u'(&(objectClass=posifxGroup)(cn=*))',
        u'starttls': False,
        u'agent-query-mode': u'SSH',
        u'load-checker': True,
    },
}

X2GO_DESKTOP_SESSIONS= [
    'KDE',
    'GNOME',
    'XFCE',
    'CINNAMON',
    'MATE',
    'XFCE',
    'LXDE',
    'TRINITY',
    'UNITY',
    'ICEWM',
    'OPENBOX',
    'XDMCP',
]

# defaults for X2Go Sessino Broker session profiles file
X2GOBROKER_SESSIONPROFILE_DEFAULTS = {
    u'DEFAULT': {
        u'command': u'TERMINAL',
        u'defsndport': True,
        u'useiconv': False,
        u'iconvfrom': u'UTF-8',
        u'height': 600,
        u'export': u'',
        u'quality': 9,
        u'fullscreen': False,
        u'layout': u'',
        u'useexports': True,
        u'width': 800,
        u'speed': 2,
        u'soundsystem': u'pulse',
        u'print': True,
        u'type': u'auto',
        u'sndport': 4713,
        u'xinerama': True,
        u'variant': u'',
        u'usekbd': True,
        u'fstunnel': True,
        u'applications': [u'TERMINAL',u'WWWBROWSER',u'MAILCLIENT',u'OFFICE'],
        u'multidisp': False,
        u'sshproxyport': 22,
        u'sound': True,
        u'rootless': True,
        u'iconvto': u'UTF-8',
        u'soundtunnel': True,
        u'dpi': 96,
        u'sshport': 22,
        u'setdpi': 0,
        u'pack': u'16m-jpeg',
        u'user': 'BROKER_USER',
        u'host': [ u'localhost', ],
        u'directrdp': False,
        u'acl-users-allow': [],
        u'acl-users-deny': [],
        u'acl-users-order': '',
        u'acl-groups-allow': [],
        u'acl-groups-deny': [],
        u'acl-groups-order': '',
        u'acl-clients-allow': [],
        u'acl-clients-deny': [],
        u'acl-clients-order': '',
        u'acl-any-order': u'deny-allow',
    },
}
