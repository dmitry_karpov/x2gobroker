# -*- coding: utf-8 -*-

# Copyright (C) 2012-2015 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# X2Go Session Broker is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# X2Go Session Broker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import os
import sys
import types
import locale
import netaddr
import distutils.version
import pwd, grp
import socket

def _checkConfigFileDefaults(data_structure):
    """\
    Check an ini-file-like data structure.

    @param data_structure: an ini-file-like data structure
    @type data_structure: C{dict} of C{dict}s

    @return: C{True} if C{data_structure} matches that of an ini file data structure
    @rtype: C{bool}

    """
    if data_structure is None:
        return False
    if type(data_structure) is not types.DictType:
        return False
    for sub_dict in data_structure.values():
        if type(sub_dict) is not types.DictType:
            return False
    return True


def touch_file(filename, mode='a'):
    """\
    Imitates the behaviour of the GNU/touch command.

    @param filename: name of the file to touch
    @type filename: C{str}
    @param mode: the file mode (as used for Python file objects)
    @type mode: C{str}

    """
    if not os.path.isdir(os.path.dirname(filename)):
        os.makedirs(os.path.dirname(filename), mode=00700)
    f = open(filename, mode=mode)
    f.close()


def get_encoding():
    """\
    Detect systems default character encoding.

    @return: The system's local character encoding.
    @rtype: C{str}

    """
    try:
        encoding = locale.getdefaultlocale()[1]
        if encoding is None:
            raise BaseException
    except:
        try:
            encoding = sys.getdefaultencoding()
        except:
            encoding = 'ascii'
    return encoding

def compare_versions(version_a, op, version_b):
    """\
    Compare <version_a> with <version_b> using operator <op>.
    In the background C{distutils.version.LooseVersion} is
    used for the comparison operation.

    @param version_a: a version string
    @type version_a: C{str}
    @param op: an operator provide as string (e.g. '<', '>', '==', '>=' etc.)
    @type op: C{str}
    @param version_b: another version string that is to be compared with <version_a>
    @type version_b: C{str}

    """

    ### FIXME: this comparison is not reliable with beta et al. version strings

    ver_a = distutils.version.LooseVersion(version_a)
    ver_b = distutils.version.LooseVersion(version_b)

    return eval("ver_a %s ver_b" % op)


def normalize_hostnames(servers):
    """\
    """

    # test the data type of servers
    arg_is_dict = False
    servers_normalized = []
    if type(servers) is types.DictType:
        arg_is_dict = True
        servers_normalized = {}
    elif type(servers) is types.TupleType:
        servers=list(servers)
    elif type(servers) not in (types.ListType, types.TupleType):
        raise ValueError('only lists, tuples and dictionaries are valid for x2gobroker.utils.normalize_hostnames()')

    subdomains = []
    for server in servers:

        # do not deal with IPv4 or IPv6 addresses
        if netaddr.valid_ipv4(server) or netaddr.valid_ipv6(server):
            continue
        else:
            _server = server
            if '.' not in _server:
                _server += '.'
            hostname, subdomain = _server.split('.', 1)
            if arg_is_dict:
                servers_normalized[hostname] = servers[server]
            else:
                servers_normalized.append(hostname)

            # collect the list of subdomains used in all server names
            if subdomain and subdomain not in subdomains:
                subdomains.append(subdomain)

    # return the original servers dict/list/tuple
    if len(subdomains) > 1:
        servers_normalized = servers

    return servers_normalized, subdomains

def matching_hostnames(server_list_a, server_list_b):

    matching_hosts = []

    ### NORMALIZE (=reduce to hostname only) server names (list A) if possible
    server_list_a_normalized, subdomains_a = normalize_hostnames(server_list_a)

    ### NORMALIZE server names (in list B), only if we have a unique domain match in list A
    if len(subdomains_a) <= 1:

        server_list_b_normalized, subdomains_b = normalize_hostnames(server_list_b)
        if len(subdomains_b) <= 1:

            if len(subdomains_a) == 0 or len(subdomains_b) == 0:
                matching_hosts = list(set(server_list_a_normalized).intersection(set(server_list_b_normalized)))

    if not matching_hosts:
        matching_hosts = list(set(server_list_a).intersection(set(server_list_b)))

    return matching_hosts

def drop_privileges(uid, gid):
    if os.getuid() != 0:
        # We're not root so, like, whatever dude
        return

    # Get the uid/gid from the name
    running_uid = pwd.getpwnam(uid).pw_uid
    running_gid = grp.getgrnam(gid).gr_gid

    # Remove group privileges
    os.setgroups([])

    # Try setting the new uid/gid
    os.setgid(running_gid)
    os.setuid(running_uid)

    # Ensure a very conservative umask
    os.umask(077)

    # set the new user's home directory as $HOME
    os.environ['HOME'] = pwd.getpwnam(uid).pw_dir

def split_host_address(host, default_address=None, default_port=22):

    if type(host) is types.IntType:
        host = unicode(host)
    # do some stripping first...
    host = host.strip()
    host = host.lstrip('*')
    host = host.lstrip(':')

    bind_address = None
    bind_port = None

    is_ipv6 = None
    if host and host[0] == '[':
        is_ipv6 = True

    if ':' in host:
        bind_address, bind_port = host.rsplit(':', 1)
        try:
            bind_port = int(bind_port)
        except ValueError:
            # obviously we split an IPv6 address
            bind_address = host
            bind_port = int(default_port)
    else:
        try:
            # in host we find a port number only
            bind_port = int(host)
        except ValueError:
            if host:
                bind_address = host
            else:
                bind_address = '0.0.0.0'
            if type(default_port) is types.IntType:
                # use the given default, in host, there is an IP address or hostname
                bind_port = default_port
            else:
                # setting a hard-coded port
                bind_port = 22

    if bind_address is None:
        # in "host" we found the bind_port, now we assign the bind_address
        bind_address = '0.0.0.0'
        if default_address:
            bind_address = default_address

    bind_address = bind_address.lstrip('[').rstrip(']')
    if is_ipv6:
        bind_address = '[{address}]'.format(address=bind_address)

    return bind_address, bind_port

def portscan(addr, port=22):
    """\
    Performing a port scan to the requested hostname.

    @param addr: address (IPv4, IPv6 or hostname) of the host
        we want to probe
    @type addr: C{unicode}
    @param port: port number (default: 22)
    @type addr: C{int}

    """
    ip_proto = 0
    try:
        socket.getaddrinfo(addr, None, socket.AF_INET6)
        ip_proto = 6
    except socket.gaierror:
        try:
            socket.getaddrinfo(addr, None, socket.AF_INET)
            ip_proto = 4
        except socket.gaierror:
            # we can't find a valid address for this host, so returning a failure...
            return False

    if ip_proto == 6 or netaddr.valid_ipv6(addr):
        sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
    elif ip_proto == 4 or netaddr.valid_ipv4(addr):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    sock.settimeout(2)
    try:
        result = sock.connect_ex((addr, port))
        if result !=0:
            sock.close()
            return False
    except socket.gaierror:
        return False
    except socket.error:
       return False
    finally:
        sock.close()

    return True
