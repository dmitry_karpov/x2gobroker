# -*- coding: utf-8 -*-

# Copyright (C) 2012-2015 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# X2Go Session Broker is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# X2Go Session Broker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

# modules
import pwd
import grp

# Python X2GoBroker modules
import base_nameservice as base

_pwd = pwd.getpwall()
_grp = grp.getgrall()

class X2GoBrokerNameService(base.X2GoBrokerNameService):

    def get_users(self):
        return [ unicode(p.pw_name) for p in _pwd ]

    def get_primary_group(self, username):
        prim_gid_number = [ p.pw_gid for p in _pwd if unicode(p.pw_name) == unicode(username) ][0]
        return [ unicode(g.gr_name) for g in _grp if g.gr_gid == prim_gid_number ][0]

    def get_groups(self):
        return [ unicode(g.gr_name) for g in _grp ]

    def get_group_members(self, group, primary_groups=False):
        _members_from_primgroups = []
        if primary_groups:
            for username in self.get_users():
                if unicode(group) == self.get_primary_group(username):
                    _members_from_primgroups.append(unicode(username))
        return [ unicode(u) for u in grp.getgrnam(group).gr_mem ] + _members_from_primgroups

