# -*- coding: utf-8 -*-

# Copyright (C) 2012-2015 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# X2Go Session Broker is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# X2Go Session Broker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

class X2GoBrokerNameService(object):

    def has_user(self, username):
        return unicode(username) in self.get_users()

    def get_users(self):
        return []

    def get_primary_group(self, username):
        return u''

    def has_group(self, group):
        return unicode(group) in self.get_groups()

    def get_groups(self):
        return []

    def is_group_member(self, username, group, primary_groups=False):
        _members = self.get_group_members(group, primary_groups=primary_groups)
        return unicode(username) in _members

    def get_group_members(self, group, primary_groups=False):
        return []

    def get_user_groups(self, username, primary_groups=False):
        _groups = []
        for _group in self.get_groups():
            if self.is_group_member(username=username, group=_group, primary_groups=primary_groups):
                _groups.append(unicode(_group))
        return _groups
