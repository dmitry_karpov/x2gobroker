# -*- coding: utf-8 -*-

# Copyright (C) 2012-2015 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# X2Go Session Broker is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# X2Go Session Broker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

# Python X2GoBroker modules
import base_nameservice as base

_users = [ u'maja', u'willi', u'flip', u'kassandra', u'thekla' ]
_groups = {
    u'male': [u'willi', u'flip'],
    u'female': [u'maja', u'kassandra', u'thekla'],
    u'bees': [u'maja', u'willi', u'kassandra'],
    u'grasshoppers': [u'flip'],
    u'spiders': [u'thekla'],
}


class X2GoBrokerNameService(base.X2GoBrokerNameService):

    def get_users(self):
        return _users

    def get_primary_group(self, username):
        return unicode(username)

    def get_groups(self):
        return _groups.keys() + _users

    def get_group_members(self, group, primary_groups=False):
        _members = []
        if group in _groups.keys():
            _members.extend(_groups[group])
        if primary_groups:
            for username in self.get_users():
                if unicode(group) == self.get_primary_group(username):
                    _members.append(username)
        return _members

