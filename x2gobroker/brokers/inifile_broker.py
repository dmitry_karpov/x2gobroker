# -*- coding: utf-8 -*-

# Copyright (C) 2012-2015 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# X2Go Session Broker is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# X2Go Session Broker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

"""\
L{inifile.X2GoBroker} class - a simple X2GoBroker implementations that uses text-based config files (also supports load balancing)

"""
__NAME__ = 'x2gobroker-pylib'

# modules
import copy
import netaddr
import re

# Python X2GoBroker modules
import base_broker as base
import x2gobroker.config
import x2gobroker.defaults
import x2gobroker.x2gobroker_exceptions

from ConfigParser import NoSectionError

class X2GoBroker(base.X2GoBroker):

    backend_name = 'inifile'

    def __init__(self, profile_config_file=None, profile_config_defaults=None, **kwargs):
        """\
        @param config_file: path to the X2Go Session Broker configuration file (x2gobroker.conf)
        @type config_file: C{unicode}
        @param profile_config_file: path to the backend's session profile configuration (x2gobroker-sessionprofiles.conf)
        @type profile_config_file: C{unicode}

        """
        base.X2GoBroker.__init__(self, **kwargs)

        if profile_config_file is None: profile_config_file = x2gobroker.defaults.X2GOBROKER_SESSIONPROFILES
        if profile_config_defaults is None: profile_config_defaults = x2gobroker.defaults.X2GOBROKER_SESSIONPROFILE_DEFAULTS
        self.session_profiles = x2gobroker.config.X2GoBrokerConfigFile(config_files=profile_config_file, defaults=profile_config_defaults)

    def get_profile_ids(self):

        return self.session_profiles.list_sections()

    def get_profile_defaults(self):

        profile_defaults = self.session_profiles.get_defaults()
        for key in profile_defaults.keys():
            if key.startswith('acl-'):
                del profile_defaults[key]
        return profile_defaults

    def get_profile(self, profile_id):

        try:
            profile = self.session_profiles.get_section(profile_id)
        except NoSectionError:
            raise x2gobroker.x2gobroker_exceptions.X2GoBrokerProfileException('No such session profile ID: {profile_id}'.format(profile_id=profile_id))

        profile_defaults = self.get_profile_defaults()
        for key in profile_defaults.keys():
            if key not in profile.keys():
                profile.update({ key: profile_defaults[key] })
        for key in profile.keys():
            if key.startswith('acl-'):
                del profile[key]
            if key.startswith('broker-'):
                del profile[key]
            if key == 'default':
                del profile[key]
            if key == 'host':
                _hosts = copy.deepcopy(profile[key])
                try: _default_sshport = int(profile['sshport'])
                except TypeError: _default_sshport = 22
                profile[key] = []
                for host in _hosts:
                    if re.match('^.*\ \(.*\)$', host):
                        _hostname = host.split(' ')[0]
                        _address = host.split(' ')[1][1:-1]
                        _address, _port = x2gobroker.utils.split_host_address(_address, default_port=_default_sshport)

                        # test if _address is a valid hostname, a valid DNS name or an IPv4/IPv6 address
                        if (re.match('(?!-)[A-Z\d-]{1,63}(?<!-)$', _hostname, flags=re.IGNORECASE) or \
                            re.match('(?=^.{1,254}$)(^(?:(?!\d|-)[a-zA-Z0-9\-]{1,63}(?<!-)\.?)+(?:[a-zA-Z]{2,})$)', _hostname, flags=re.IGNORECASE)) and \
                           (re.match('(?=^.{1,254}$)(^(?:(?!\d|-)[a-zA-Z0-9\-]{1,63}(?<!-)\.?)+(?:[a-zA-Z]{2,})$)', _address, flags=re.IGNORECASE) or \
                            netaddr.valid_ipv4(_address) or netaddr.valid_ipv6(_address)):

                            profile["host={hostname}".format(hostname=_hostname)] = _address
                            if _port != _default_sshport:
                                profile["sshport={hostname}".format(hostname=_hostname)] = _port

                        profile[key].append(_hostname)
                    else:
                        profile[key].append(host)
        return profile

    def get_profile_broker(self, profile_id):

        profile = self.session_profiles.get_section(profile_id)
        for key in profile.keys():
            if not key.startswith('broker-'):
                del profile[key]
            if key.startswith('broker-') and (profile[key] == '' or profile[key] == ['']):
                del profile[key]
        return profile

    def get_profile_acls(self, profile_id):

        profile = self.session_profiles.get_section(profile_id)
        for key in profile.keys():
            if not key.startswith('acl-'):
                del profile[key]
            if key.startswith('acl-') and (profile[key] == '' or profile[key] == ['']):
                del profile[key]
        return profile
