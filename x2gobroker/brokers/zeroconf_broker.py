# -*- coding: utf-8 -*-

# Copyright (C) 2012-2015 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# X2Go Session Broker is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# X2Go Session Broker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

"""\
L{zeroconf.X2GoBroker} class - a demo X2GoBroker implementations that needs not configuration at all

"""
__NAME__ = 'x2gobroker-pylib'

# modules
import uuid

# Python X2GoBroker modules
import base_broker as base

class X2GoBroker(base.X2GoBroker):

    backend_name = 'zeroconf'

    def list_profiles(self, username):

        _list_of_profiles = {
            uuid.uuid4(): {
                u'user': u'',
                u'defsndport': True,
                u'useiconv': False,
                u'iconvfrom': u'UTF-8',
                u'height': 600,
                u'export': u'',
                u'quality': 9,
                u'fullscreen': False,
                u'layout': u'',
                u'useexports': 1,
                u'width': 800,
                u'speed': 2,
                u'soundsystem': u'pulse',
                u'print': True,
                u'type': u'auto',
                u'sndport': 4713,
                u'xinerama': True,
                u'variant': u'',
                u'usekbd': True,
                u'fstunnel': True,
                u'applications': [u'TERMINAL',u'WWWBROWSER',u'MAILCLIENT',u'OFFICE',],
                u'host': u'localhost',
                u'multidisp': 0,
                u'sshproxyport': 22,
                u'sound': True,
                u'rootless': 0,
                u'name': u'LOCALHOST',
                u'iconvto': u'UTF-8',
                u'soundtunnel': True,
                u'command': self.get_backend_value('broker_{backend}'.format(backend=self.backend_name), u'desktop-shell').upper(),
                u'dpi': 96,
                u'sshport': 22,
                u'setdpi': 0,
                u'pack': u'16m-jpeg',
            },
        }
        list_of_profiles = {}
        for profile_id in _list_of_profiles.keys():
            profile = self.get_profile_defaults()
            profile.update(_list_of_profiles[profile_id])
            list_of_profiles[profile_id] = profile
        return list_of_profiles

    def select_session(self, profile_id, username=None, **kwargs):

        selectprofile_output = {
            'server': 'localhost',
            'port': 22,
        }
        return selectprofile_output
