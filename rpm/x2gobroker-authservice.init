#!/bin/sh
#
# x2gobroker-authservice - Starts/stop the "x2gobroker-authservice" daemon
#
# chkconfig:   2345 99 1
# description: X2Go Session Broker's (PAM) Authentication Service

### BEGIN INIT INFO
# Provides: x2gobroker-authservice
# Required-Start: $local_fs
# Required-Stop: $local_fs
# Default-Start: 2345
# Default-Stop: 016
# Short-Description: X2Go Session Broker PAM Authentication Service
# Description:       PAM authentication service for X2Go Session Broker
### END INIT INFO

# Source function library.
. /etc/rc.d/init.d/functions

set -e

AUTHSERVICE=/usr/sbin/x2gobroker-authservice
test -d /run && RUNDIR=/run || RUNDIR=/var/run
PIDFILE_AUTHSERVICE=$RUNDIR/x2gobroker/x2gobroker-authservice.pid
DEFAULTCONFIG_COMMON=/etc/default/python-x2gobroker
DEFAULTCONFIG_AUTHSERVICE=/etc/default/x2gobroker-authservice

test -x "$AUTHSERVICE" || exit 0

START_AUTHSERVICE=false
X2GOBROKER_DEBUG=0
X2GOBROKER_DAEMON_USER='x2gobroker'
X2GOBROKER_DAEMON_GROUP='x2gobroker'
X2GOBROKER_AUTHSERVICE_SOCKET="$RUNDIR/x2gobroker/x2gobroker-authservice.socket"
test -f $DEFAULTCONFIG_COMMON && . $DEFAULTCONFIG_COMMON
test -f $DEFAULTCONFIG_AUTHSERVICE && . $DEFAULTCONFIG_AUTHSERVICE

if ! getent passwd $X2GOBROKER_DAEMON_USER 1>/dev/null 2>/dev/null; then
        X2GOBROKER_DAEMON_USER=nobody
fi
if ! getent group $X2GOBROKER_DAEMON_GROUP 1>/dev/null 2>/dev/null; then
        X2GOBROKER_DAEMON_GROUP=nobody
fi

export X2GOBROKER_DEBUG
export X2GOBROKER_DAEMON_USER
export X2GOBROKER_DAEMON_GROUP
export X2GOBROKER_AUTHSERVICE_SOCKET

exec=$AUTHSERVICE
prog=$(basename $AUTHSERVICE)
config=$DEFAULTCONFIG_AUTHSERVICE
OPTS="-D -P $PIDFILE_AUTHSERVICE -s $X2GOBROKER_AUTHSERVICE_SOCKET -o root -g $X2GOBROKER_DAEMON_GROUP -p 0660"

lockfile=/var/lock/subsys/$prog


is_true()
{
	case "${1:-}" in
		[Yy]es|[Yy]|1|[Tt]|[Tt]rue) return 0;;
		*) return 1;
	esac
}


start() {
[ -x $exec ] || exit 5
	if is_true $START_AUTHSERVICE; then
		# Make sure these are created by default so that nobody else can
		echo -n $"Starting $prog: "
		set +e
		daemon $exec $OPTS
		retval=$?
		set -e
		echo
		[ $retval -eq 0 ] && touch $lockfile
	fi
}


stop() {
	echo -n $"Stopping $prog: "
	set +e
	killproc -p $PIDFILE_AUTHSERVICE $exec
	retval=$?
	set -e
	echo
	rm -f $lockfile
	return $retval
}


restart() {
	stop
	start
}


reload() {
	restart
}


force_reload() {
	restart
}


rh_status() {
	# run checks to determine if the service is running or use generic status
	status -p $PIDFILE_AUTHSERVICE $exec
}


rh_status_q() {
	rh_status 1>/dev/null 2>&1
}


case "$1" in
	start)
		set +e
		rh_status_q && exit 0
		set -e
		$1
		;;
	stop)
		set +e
		rh_status_q || exit 0
		set -e
		$1
		;;
	restart)
		$1
		;;
	reload)
		set +e
		rh_status_q || exit 7
		set -e
		$1
		;;
	force-reload)
		force_reload
		;;
	status)
		set +e
		rh_status
		set -e
		;;
	condrestart|try-restart)
		set +e
		rh_status_q || exit 0
		set -e
		restart
		;;
	*)
		echo $"Usage: $0 {start|stop|status|restart|condrestart|try-restart|reload|force-reload}"
		exit 2
		;;
esac
exit $?
