#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2014-2015 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# X2Go Session Broker is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# X2Go Session Broker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

"""
Unit tests for Python X2GoBroker.
"""
import os

if __name__ == "__main__":
    os.environ.update({'X2GOBROKER_DEBUG': "1"})
    os.environ.update({'X2GOBROKER_TESTSUITE': "1"})
    os.chdir(os.path.join('x2gobroker', 'tests',))
    os.system('python ./runalltests.py')
